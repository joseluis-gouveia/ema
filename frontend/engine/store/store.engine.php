<?php

	require_once($_SERVER['DOCUMENT_ROOT']."/ema/backend/cfg/config.inc.php");

	extract($_POST);
	$orderCart;

	switch($action)
	{
		case "currentCart":
			$orderCart = StoreManager::currentCart();
			echo $orderCart;
		break;

		case "clearCart":
			StoreManager::clearCart();
		break;

		case "getFullCart":
			echo json_encode(StoreManager::getFullCart());
		break;

		case "addProduct":
			echo json_encode(StoreManager::addProduct($productID, $productQuantity));
		break;

		case "deleteProduct":
			echo json_encode(StoreManager::deleteProduct($productID));
		break;

		case "addCustomProduct": $productVO = new ContentVO($productVO); echo
		json_encode(StoreManager::addCustomProduct($productVO)); break;

		case "updateProductQuantity":
			echo json_encode(StoreManager::updateProductQuantity($productID, $quantity));
		break;

		case "updateProductQuantityByProductID":
			echo json_encode(StoreManager::updateProductQuantityByProductID($productID, $quantity));
		break;


		case "calculateWeight":
			StoreManager::updateShippingCountry($destinyCountry);
			echo json_encode(StoreManager::calculateWeight($destinyCountry, $weight, $totalCost));
		break;

		case "updateShippingCountry":
			echo json_encode(StoreManager::updateShippingCountry($destinyCountry));
		break;

		case "updateUserID":

			StoreManager::updateUserID($userID);
		break;

		case "setOrder":
			if(!isset($duploOptin)) { $duploOptin = true; } else { $duploOptin = $duploOptin === 'true'? true: false; }
			if(!isset($sendRegisterToAdmin)) { $sendRegisterToAdmin = true; } else { $sendRegisterToAdmin = $sendRegisterToAdmin === 'true'? true: false; }
			if(!isset($allowDuplicateEmail)) { $allowDuplicateEmail = false; } else { $allowDuplicateEmail = $allowDuplicateEmail === 'true'? true: false; }

			if(!isset($user->parentID)) { $user->parentID = 0; }
			if(!isset($returnLabelTypeSucess)) { $returnLabelTypeSucess = "REGISTERSUCCESS"; }
			if(!isset($registerConfirmationSubject)) { $registerConfirmationSubject = "REGISTERCONFIRMATIONSUBJECT"; }
			if(!isset($registerConfirmationBody)) { $registerConfirmationBody = "REGISTERCONFIRMATIONSBODY"; }

			$user = new UserVO($_POST);

			echo json_encode(StoreManager::setOrder($user, $allowDuplicateEmail, $duploOptin));
		break;

		case "getAllOrders":
			echo json_encode(StoreManager::getAllOrders($userID));
		break;



	}




?>
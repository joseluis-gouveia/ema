<?php
require_once("../../backend/cfg/config.inc.php");
session_start();
if (isset($_SESSION["userID"])) {
    if (AuthorizationManager::hasAuthorization("admin", $_SESSION["userID"])) {
        header('Content-Type: text/html; charset=utf-8');

        $fileName = "[" . date('d-m-Y') . "].csv";

        if (isset($_GET["fileName"])) {
            if ($_GET["fileName"] != "") {
                $fileName = $_GET["fileName"];
            }
        }

        $newCall = "SELECT firstName, lastName, email, phone, country, notes as origin, subscribeDate  FROM usersWebsite WHERE ";
        $categories = UtilsManager::mergeArrayChildren($_GET["categories"]);
        for ($i = 0; $i < count($categories); $i++) {
            $newCall .= " LOWER(notes) LIKE '%" . mb_strtolower($categories[$i], "UTF-8") . "%' ";
            if ($i < count($categories) - 1) {
                $newCall .= " OR ";
            }
        }
        $newCall .= " ORDER BY subscribeDate DESC";
        //echo $newCall;

        $res = BdConn::runSQL($newCall);
        //echo $newCall;
        UtilsManager::exportCSV($res, $fileName);
    }
}

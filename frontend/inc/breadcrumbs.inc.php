<?php
$breadcrumbs = "";

for($i = 0; $i < returnURLLevels()-1; ++$i)
{

	if(ContentsManager::getContentByDeeplink(getURLLevel($i+1))->menuLabel != $title)
	{
		if($i < returnURLLevels()-1 && $i > 0)
		{
			$breadcrumbs .=  " / ";	
		}
		
		if(ContentsManager::getContentByDeeplink(getURLLevel($i+1))->deeplink)
		{
			if($i >=1)
			{
				$breadcrumbs .= "<span>".ContentsManager::getContentByDeeplink(getURLLevel($i+1))->menuLabel."</span>";	
			}
			else
			{
				$breadcrumbs .= ContentsManager::getContentByDeeplink(getURLLevel($i+1))->menuLabel;
			}
			
		}
		else
		{
			$breadcrumbs .= ContentsManager::getHomeContent(0)->menuLabel;
		}
		
	}
}
echo $breadcrumbs;
?>
<?php

include_once('../../../dbconfig.php');
require("../../backend/cfg/config.inc.php");
//var_dump(get_included_files());
/* Framework init */
ini_set('error_reporting', 0);
ini_set('display_errors', 0);
if (isset($_COOKIE['languageID'])) {
	$languageID = $_COOKIE['languageID'];
	$language = LanguageManager::getLanguage($languageID);
} else {
	if (!$language->ID) {
		$language = LanguageManager::getDefaultLanguage();
	}
	$languageID = $language->ID;
}
setlocale(LC_ALL, $language->translateCode);
?>

<?php


$userVO = new UserVO();
$userVO->email = trim($_GET['email']);
$userVO->activationCode = trim($_GET['activation_code']);

$myOperationEmailManager = new OperationEmailManager;

$msg = UserManager::sendNewPassword($userVO, $_GET['language']);
echo $myOperationEmailManager->getOperationalEmail(LabelsManager::getLabelValue("SYSTEMUSERRECOVERPASSWORDBODYSUCCESS", $_GET['language']));



?>
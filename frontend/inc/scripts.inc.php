<script src="/ema/frontend/scripts/add-ins/addins.min.js"></script>
<script src="/ema/frontend/scripts/core/ema.min.js"></script>
<script>
	$('document').ready(function(){
		$('.lang-selector a').click(function(){
			setLanguageCookie(<?php if($content->UID) {echo $content->UID;} else {echo "0";} ?>, $(this).attr('data-language-id'));
		});
	})
</script>
<?php echo $settings->analytics; ?>
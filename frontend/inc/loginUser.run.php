<?php


require_once("../../backend/cfg/config.inc.php");
if(!isset($lang))
{
	$lang = LanguageManager::getDefaultLanguage()->ID;
}
else
{
	if(!is_numeric($lang))
	{
		$lang = LanguageManager::getLanguageIDByShort($lang)->ID;
	}
}
$userVO = new UserVO();
$userVO->email = trim($_GET['username']);
$userVO->password = trim($_GET['password']);

$userVO = UserManager::login($userVO);

if(!$lang)
{
	$lang = LanguageManager::getDefaultLanguage()->ID;
}
else
{
	if(!is_numeric($lang))
	{
		$lang = LanguageManager::getLanguageIDByShort($lang)->ID;
	}
}

if(isset($userVO->label)) {
	header('Location: ' . $_SERVER['HTTP_REFERER']."&status=".$userVO->label."&msgType=".$userVO->kind);
} else {
	header('Location: ' . $_SERVER['HTTP_REFERER']);
}


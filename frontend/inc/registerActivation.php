<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<?php

require("../../backend/cfg/config.inc.php");

$sendPassword = true;
if(isset($_GET['sendPassword'])) {
	$sendPassword = (boolean) $_GET['sendPassword'];
}

$isNewsletter = true;
if(isset($_GET['isNewsletter'])) {
	$isNewsletter = (boolean) $_GET['isNewsletter'];
}

$msg = UserManager::activateSubscription($_GET['email'], $_GET['activation_code'], $_GET['language'], $sendPassword, $isNewsletter);

echo OperationEmailManager::getOperationalEmail($msg);



?>

<script type="text/javascript">
	function timerClose()
	{
		var t=setTimeout("closeWindow()",3000);
	}
	function closeWindow()
	{
		window.close();
	}
	timerClose();
</script>

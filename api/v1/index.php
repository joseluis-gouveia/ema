<?php

header('Content-Type: application/json; charset=utf-8');

// requires config file
require("../../backend/cfg/config.inc.php");

$ema = EmaBrain::Instance();
if (defined("ALLOW_GET_USERS")) {
    $ema::$ALLOW_GET_USERS = ALLOW_GET_USERS;
}
if (defined("ALLOW_EDIT_USERS")) {
    $ema::$ALLOW_EDIT_USERS = ALLOW_EDIT_USERS;
}
if (defined("ALLOW_ADD_CONTENT_NO_SESSION")) {
    $ema::$ALLOW_ADD_CONTENT_NO_SESSION = ALLOW_ADD_CONTENT_NO_SESSION;
}
if (defined("ALLOW_ADD_USERS_RELATIONS")) {
    $ema::$ALLOW_ADD_USERS_RELATIONS = ALLOW_ADD_USERS_RELATIONS;
}

global $ema;
global $settings;
global $languageID;
global $menu;
global $content;
global $labels;
global $activationCode;
global $selectedID;
global $language;
global $userVO;
global $cart;
global $country;
global $allConvertedData;


switch ($_SERVER['REQUEST_METHOD']) {
    case 'GET':
        $the_request = &$_GET;
        $break_vars = explode("/", $the_request['id']);

        break;
    case 'POST':
        $the_request = &$_GET;
        $break_vars = explode("/", $the_request['id']);
        break;

    default:
}

switch ($break_vars[0]) {
    case "content":

        if (!isset($break_vars[1])) {
            $contents = ContentsManager::getChildren(0);
            echo json_encode($contents);
        } else {
            $content_id = $break_vars[1];
            $content = ContentsManager::getContent($content_id, null, true);
            echo json_encode($content);
        }
        break;
    case "shopkit":
        if (isset($break_vars[1])) {

            switch ($break_vars[1]) {
                case "order_created":
                    $data = file_get_contents("php://input");
                    $order = json_decode($data)->order;
                    $userVO = new UserVO();
                    $userVO->email = $order->client->email;
                    $userVO->firstName = $order->client->name;
                    $userVO->address = $order->client->delivery->address;
                    $userVO->country = $order->client->delivery->country;
                    $userVO->alternativeZipcode = $order->client->delivery->zip_code;
                    $userVO->zipcodeZone = $order->client->delivery->city;
                    $userVO->phone = $order->client->delivery->phone;

                    if (count($order->products)) {
                        for ($i = 0; $i < count($order->products); $i++) {
                            $prod = $order->products[$i];
                            $uid = ContentsManager::getUIDByVerboseID($prod->id);

                            if ($uid) {

                                if (UserManager::setUserToContent($userVO, $uid)) {
                                    http_response_code(200);
                                }
                            }
                        }
                    }
                    break;
                case "order_paid":
                    $data = file_get_contents("php://input");
                    $order = json_decode($data)->order;
                    $userVO = new UserVO();
                    $userVO->email = $order->client->email;
                    $userVO->firstName = $order->client->name;
                    $userVO->address = $order->client->delivery->address;
                    $userVO->country = $order->client->delivery->country;
                    $userVO->alternativeZipcode = $order->client->delivery->zip_code;
                    $userVO->zipcodeZone = $order->client->delivery->city;
                    $userVO->phone = $order->client->delivery->phone;

                    if (count($order->products)) {
                        for ($i = 0; $i < count($order->products); $i++) {
                            $prod = $order->products[$i];
                            $uid = ContentsManager::getUIDByVerboseID($prod->id);
                            if ($uid) {
                                $data = UserManager::setUserToContent($userVO, $uid);
                                $userVO = $data->data[0];
                                // var_dump($userVO);
                                if ($uid) {

                                    if (StoreManager::setManualOrder($userVO->ID, $uid)) {
                                        CirculoSender::SendWarning("COURSESUBSCRIPTIONCONFIRMATIONSUBJECT", $uid, $userVO->ID, "COURSESUBSCRIPTIONCONFIRMATIONBODY");
                                        http_response_code(200);
                                    }
                                }
                            }
                        }
                    }
                    break;
                default:
                    echo "no request";
                    break;
            }
        }
        break;
    default:
        echo "no request";
        break;
}

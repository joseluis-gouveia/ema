<?php
ini_set('display_errors', 1);
error_reporting(E_ALL);

date_default_timezone_set('UTC');
session_start();



// requires config file
require("backend/cfg/config.inc.php");

$ema = EmaBrain::Instance();
if (defined("ALLOW_GET_USERS")) {
	$ema::$ALLOW_GET_USERS = ALLOW_GET_USERS;
}
if (defined("ALLOW_EDIT_USERS")) {
	$ema::$ALLOW_EDIT_USERS = ALLOW_EDIT_USERS;
}
if (defined("ALLOW_ADD_CONTENT_NO_SESSION")) {
	$ema::$ALLOW_ADD_CONTENT_NO_SESSION = ALLOW_ADD_CONTENT_NO_SESSION;
}
if (defined("ALLOW_ADD_USERS_RELATIONS")) {
	$ema::$ALLOW_ADD_USERS_RELATIONS = ALLOW_ADD_USERS_RELATIONS;
}

global $ema;
global $settings;
global $languageID;
global $menu;
global $content;
global $labels;
global $activationCode;
global $selectedID;
global $language;
global $userVO;
global $cart;
global $country;
global $allConvertedData;
global $locale;

$locale = Locale::acceptFromHttp($_SERVER['HTTP_ACCEPT_LANGUAGE']);
$databaseVersion = SettingsManager::getDatabaseVersion();



if ($databaseVersion < 1 || $databaseVersion == NULL) {

	BdConn::runSql('ALTER TABLE contents MODIFY locked MEDIUMINT(9) default 0;');
	BdConn::runSql('ALTER TABLE contentsToContents MODIFY sortOrder TINYINT(4) default 0;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY firstName varchar(100) null;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY lastName varchar(100) null;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY email varchar(100) null;');
	if (!count($bdconn->query("SHOW COLUMNS FROM `usersWebsite` LIKE 'country'")->fetchAll())) {
		BdConn::runSql('ALTER TABLE usersWebsite ADD country varchar(50) null;');
	}
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY country varchar(50) null;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY birthday DATE default null;');
	BdConn::runSql('UPDATE usersWebsite SET birthday = NULL WHERE birthday = "0000-00-00"');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY address varchar(100) null;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY zipcode4 varchar(4) null;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY zipcode3 varchar(3) null;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY zipcodeZone varchar(100) null;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY alternativeZipcode varchar(100) null;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY job varchar(100) null;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY shippingContactFirstName varchar(100) null;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY shippingContactLastName varchar(100) null;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY shippingAddress varchar(100) null;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY shippingZipcode4 varchar(4) null;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY shippingZipcode3 varchar(3) null;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY shippingZipcodeZone varchar(100) null;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY shippingAlternativeZipcode varchar(100) null;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY shippingCountry varchar(50) null;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY invoiceFirstName varchar(100) null;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY invoiceLastName varchar(100) null;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY invoiceNif varchar(20) null;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY invoiceAddress varchar(100) null;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY invoiceZipcode4 varchar(4) null;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY invoiceZipcode3 varchar(3) null;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY invoiceZipcodeZone varchar(100) null;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY invoiceAlternativeZipcode varchar(100) null;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY invoiceCountry varchar(50) null;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY phone varchar(20) null;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY mobile varchar(20) null;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY optional1 varchar(255) null;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY optional2 varchar(255) null;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY optional3 varchar(255) null;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY optional4 varchar(255) null;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY optional5 varchar(255) null;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY optional6 varchar(255) null;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY optional7 varchar(255) null;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY optional8 varchar(255) null;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY optional9 varchar(255) null;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY optional10 varchar(255) null;');
	if (!count($bdconn->query("SHOW COLUMNS FROM `usersWebsite` LIKE 'gender'")->fetchAll())) {
		BdConn::runSql('ALTER TABLE usersWebsite ADD gender enum("f","m") default null;');
	}
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY gender enum("f","m") default null;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY nif varchar(20) null;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY newsletter int(11) default 0;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY partnersPub int(11) default 0;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY notes text default null;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY password varchar(100) null;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY facebookID varchar(255) null;');

	BdConn::runSql('UPDATE usersWebsite SET subscribeDate = NULL WHERE subscribeDate = "0000-00-00"');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY subscribeDate DATETIME default null;');

	BdConn::runSql('UPDATE usersWebsite SET lastLoginDate = NULL WHERE lastLoginDate = "0000-00-00"');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY lastLoginDate DATETIME default null;');

	BdConn::runSql('UPDATE usersWebsite SET editDate = NULL WHERE editDate = "0000-00-00"');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY editDate DATETIME default null;');

	BdConn::runSql('UPDATE usersWebsite SET expireDate = NULL WHERE expireDate = "0000-00-00"');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY expireDate DATETIME default null;');

	BdConn::runSql('ALTER TABLE usersWebsite MODIFY activationCode varchar(100) null;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY active int(11) default 0;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY approved int(11) default 0;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY enabled int(11) default 1;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY parentID int(11) default 0;');

	BdConn::runSql('ALTER TABLE log MODIFY notes text default null;');
	if (!count($bdconn->query("SHOW COLUMNS FROM `log` LIKE 'sessionID'")->fetchAll())) {
		BdConn::runSql('ALTER TABLE log add sessionID varchar(250) null;');
	}
	if (!count($bdconn->query("SHOW COLUMNS FROM `log` LIKE 'referer'")->fetchAll())) {
		BdConn::runSql('ALTER TABLE log add referer varchar(250) null;');
	}
	if (!count($bdconn->query("SHOW COLUMNS FROM `labels` LIKE 'editable'")->fetchAll())) {
		BdConn::runSql('ALTER TABLE labels add editable varchar(250) null;');
	}
	BdConn::runSql('UPDATE settings SET databaseVersion  = 1');
}

if ($databaseVersion < 1.1) {
	BdConn::runSql('ALTER TABLE storeCartsProducts MODIFY customTitle varchar(255) null;');
	BdConn::runSql('ALTER TABLE storeCartsProducts MODIFY customImage varchar(255) null;');
	BdConn::runSql('ALTER TABLE storeCartsProducts MODIFY customImage varchar(255) null;');
	BdConn::runSql('ALTER TABLE storeCartsProducts MODIFY notes text default null;');
	BdConn::runSql('UPDATE settings SET databaseVersion  = 1.1');
}

if ($databaseVersion < 1.2) {
	$val = BdConn::tableExists('storeSettings');

	if ($val === FALSE) {
		BdConn::runSQL('CREATE TABLE `storeSettings` (
	  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
	  `access_token` text,
	  `refresh_token` text,
	  `expires_in` int(11) DEFAULT NULL,
	  `edit_date` datetime DEFAULT NULL,
	  `action` varchar(255) DEFAULT NULL,
	  	PRIMARY KEY (`id`)
		) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;');
	}


	if (!count($bdconn->query("SHOW COLUMNS FROM `storeCarts` LIKE 'editDate'")->fetchAll())) {
		BdConn::runSql('ALTER TABLE storeCarts ADD editDate DATETIME default null;');
	}
	if (!count($bdconn->query("SHOW COLUMNS FROM `storeCarts` LIKE 'shippingCustomValue'")->fetchAll())) {
		BdConn::runSql('ALTER TABLE storeCarts ADD shippingCustomValue float default null;');
	}

	if (!count($bdconn->query("SHOW COLUMNS FROM `storeOrders` LIKE 'editDate'")->fetchAll())) {
		BdConn::runSql('ALTER TABLE storeOrders ADD editDate DATETIME default null;');
	}

	if (!count($bdconn->query("SHOW COLUMNS FROM `storeOrders` LIKE 'shippingCustomValue'")->fetchAll())) {
		BdConn::runSql('ALTER TABLE storeOrders ADD shippingCustomValue float default null;');
	}
	BdConn::runSql('UPDATE settings SET databaseVersion  = 1.2');
}
if ($databaseVersion < 1.3) {

	BdConn::runSql('ALTER TABLE contentsInfo MODIFY optional1 text default null;');
	BdConn::runSql('ALTER TABLE contentsInfo MODIFY optional2 text default null;');
	BdConn::runSql('ALTER TABLE contentsInfo MODIFY optional3 text default null;');
	BdConn::runSql('ALTER TABLE contentsInfo MODIFY optional4 text default null;');
	BdConn::runSql('ALTER TABLE contentsInfo MODIFY optional5 text default null;');
	BdConn::runSql('ALTER TABLE contentsInfo MODIFY optional6 text default null;');
	BdConn::runSql('ALTER TABLE contentsInfo MODIFY optional7 text default null;');
	BdConn::runSql('ALTER TABLE contentsInfo MODIFY optional8 text default null;');
	BdConn::runSql('ALTER TABLE contentsInfo MODIFY optional9 text default null;');
	BdConn::runSql('ALTER TABLE contentsInfo MODIFY optional10 text default null;');

	BdConn::runSql('UPDATE settings SET databaseVersion  = 1.3');
}
if ($databaseVersion < 1.4) {

	if (!count($bdconn->query("SHOW COLUMNS FROM `contents` LIKE 'verboseID'")->fetchAll())) {
		BdConn::runSql('ALTER TABLE contents ADD verboseID varchar(255) null;');
	}

	BdConn::runSql('UPDATE settings SET databaseVersion  = 1.4');
}
if ($databaseVersion < 1.5) {
	$val = BdConn::tableExists('redirects');

	if ($val === FALSE) {
		BdConn::runSQL('CREATE TABLE `redirects` (
		`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
		`url` varchar(250) DEFAULT NULL,
		`new_url` varchar(250) DEFAULT NULL,
		`redirect_code` int(11) DEFAULT NULL,
		PRIMARY KEY (`id`)
	  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;');
	}


	BdConn::runSql('UPDATE settings SET databaseVersion  = 1.5');
}

if ($databaseVersion < 1.6) {
	if (!count($bdconn->query("SHOW COLUMNS FROM `relations` LIKE 'tag'")->fetchAll())) {
		BdConn::runSql('ALTER TABLE relations ADD tag varchar(255) null;');
		BdConn::runSql('alter table relations drop index parentID;');
		BdConn::runSql('ALTER TABLE relations ADD UNIQUE (parentID, childID, parentTable, childTable, tag);');
	}
	BdConn::runSql('UPDATE settings SET databaseVersion  = 1.6');
}

if ($databaseVersion < 1.6) {
	if (!count($bdconn->query("SHOW COLUMNS FROM `relations` LIKE 'tag'")->fetchAll())) {
		BdConn::runSql('ALTER TABLE relations ADD tag varchar(255) null;');
		BdConn::runSql('alter table relations drop index parentID;');
		BdConn::runSql('ALTER TABLE relations ADD UNIQUE (parentID, childID, parentTable, childTable, tag);');
	}
	BdConn::runSql('UPDATE settings SET databaseVersion  = 1.6');
}

if ($databaseVersion < 1.7) {
	BdConn::runSql('ALTER TABLE relations MODIFY parentID int default null;');
	BdConn::runSql('ALTER TABLE relations MODIFY childID int default null;');
	BdConn::runSql('ALTER TABLE relations MODIFY sortOrder int default null;');

	BdConn::runSql('UPDATE settings SET databaseVersion  = 1.7');
}

if ($databaseVersion < 1.8) {
	BdConn::runSql('DROP TABLE `contentsRolesPermissions`');
	BdConn::runSql('CREATE TABLE `rolesPermissions` (
		`ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
		`name` varchar(255) NOT NULL,
		`description` varchar(255) DEFAULT NULL,
		PRIMARY KEY (`ID`),
		UNIQUE KEY `name` (`name`)
	  );');

	BdConn::runSql('UPDATE settings SET databaseVersion  = 1.8');
}

if ($databaseVersion < 1.9) {
	if (!count($bdconn->query("SHOW COLUMNS FROM `ip2nationCountries` LIKE 'phonecode'")->fetchAll())) {
		BdConn::runSql('ALTER TABLE ip2nationCountries ADD phonecode bigint default null;');
	}
	BdConn::runSql('UPDATE ip2nationCountries set `iso_country` =  "Korea, Democratic People\'s Republic of" WHERE `iso_country` = "Korea, Democratic Peopleâ€™s Republic of (North)";');
	BdConn::runSql('UPDATE ip2nationCountries set `iso_country` =  "Lao People\'s Democratic Republic" WHERE `iso_country` = "Lao Peopleâ€™s Democratic Republic";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  93 WHERE `iso_country` = "Afghanistan";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  355 WHERE `iso_country` = "Albania";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  213 WHERE `iso_country` = "Algeria";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  1684 WHERE `iso_country` = "American Samoa";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  376 WHERE `iso_country` = "Andorra";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  244 WHERE `iso_country` = "Angola";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  1264 WHERE `iso_country` = "Anguilla";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  0 WHERE `iso_country` = "Antarctica";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  1268 WHERE `iso_country` = "Antigua and Barbuda";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  54 WHERE `iso_country` = "Argentina";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  374 WHERE `iso_country` = "Armenia";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  297 WHERE `iso_country` = "Aruba";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  61 WHERE `iso_country` = "Australia";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  43 WHERE `iso_country` = "Austria";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  994 WHERE `iso_country` = "Azerbaijan";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  1242 WHERE `iso_country` = "Bahamas";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  973 WHERE `iso_country` = "Bahrain";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  880 WHERE `iso_country` = "Bangladesh";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  1246 WHERE `iso_country` = "Barbados";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  375 WHERE `iso_country` = "Belarus";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  32 WHERE `iso_country` = "Belgium";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  501 WHERE `iso_country` = "Belize";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  229 WHERE `iso_country` = "Benin";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  1441 WHERE `iso_country` = "Bermuda";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  975 WHERE `iso_country` = "Bhutan";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  591 WHERE `iso_country` = "Bolivia";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  387 WHERE `iso_country` = "Bosnia and Herzegovina";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  267 WHERE `iso_country` = "Botswana";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  0 WHERE `iso_country` = "Bouvet Island";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  55 WHERE `iso_country` = "Brazil";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  246 WHERE `iso_country` = "British Indian Ocean Territory";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  673 WHERE `iso_country` = "Brunei Darussalam";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  359 WHERE `iso_country` = "Bulgaria";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  226 WHERE `iso_country` = "Burkina Faso";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  257 WHERE `iso_country` = "Burundi";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  855 WHERE `iso_country` = "Cambodia";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  237 WHERE `iso_country` = "Cameroon";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  1 WHERE `iso_country` = "Canada";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  238 WHERE `iso_country` = "Cape Verde";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  1345 WHERE `iso_country` = "Cayman Islands";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  236 WHERE `iso_country` = "Central African Republic";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  235 WHERE `iso_country` = "Chad";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  56 WHERE `iso_country` = "Chile";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  86 WHERE `iso_country` = "China";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  61 WHERE `iso_country` = "Christmas Island";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  672 WHERE `iso_country` = "Cocos (Keeling) Islands";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  57 WHERE `iso_country` = "Colombia";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  269 WHERE `iso_country` = "Comoros";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  242 WHERE `iso_country` = "Congo";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  242 WHERE `iso_country` = "Congo, Democratic Republic of the";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  682 WHERE `iso_country` = "Cook Islands";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  506 WHERE `iso_country` = "Costa Rica";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  225 WHERE `iso_country` = "Cote D\'Ivoire";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  385 WHERE `iso_country` = "Croatia (Hrvatska)";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  53 WHERE `iso_country` = "Cuba";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  357 WHERE `iso_country` = "Cyprus";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  420 WHERE `iso_country` = "Czech Republic";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  45 WHERE `iso_country` = "Denmark";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  253 WHERE `iso_country` = "Djibouti";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  1767 WHERE `iso_country` = "Dominica";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  1809 WHERE `iso_country` = "Dominican Republic";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  593 WHERE `iso_country` = "Ecuador";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  20 WHERE `iso_country` = "Egypt";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  503 WHERE `iso_country` = "El Salvador";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  240 WHERE `iso_country` = "Equatorial Guinea";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  291 WHERE `iso_country` = "Eritrea";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  372 WHERE `iso_country` = "Estonia";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  251 WHERE `iso_country` = "Ethiopia";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  500 WHERE `iso_country` = "Falkland Islands (Malvinas)";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  298 WHERE `iso_country` = "Faroe Islands";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  679 WHERE `iso_country` = "Fiji";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  358 WHERE `iso_country` = "Finland";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  358 WHERE `iso_country` = "Aland Islands";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  33 WHERE `iso_country` = "France";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  594 WHERE `iso_country` = "French Guiana";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  689 WHERE `iso_country` = "French Polynesia";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  0 WHERE `iso_country` = "French Southern Territories";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  241 WHERE `iso_country` = "Gabon";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  220 WHERE `iso_country` = "Gambia";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  995 WHERE `iso_country` = "Georgia";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  49 WHERE `iso_country` = "Germany";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  233 WHERE `iso_country` = "Ghana";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  350 WHERE `iso_country` = "Gibraltar";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  30 WHERE `iso_country` = "Greece";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  299 WHERE `iso_country` = "Greenland";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  1473 WHERE `iso_country` = "Grenada";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  590 WHERE `iso_country` = "Guadeloupe";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  1671 WHERE `iso_country` = "Guam";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  502 WHERE `iso_country` = "Guatemala";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  224 WHERE `iso_country` = "Guinea";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  245 WHERE `iso_country` = "Guinea-Bissau";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  592 WHERE `iso_country` = "Guyana";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  509 WHERE `iso_country` = "Haiti";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  0 WHERE `iso_country` = "Heard Island and Mcdonald Islands";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  39 WHERE `iso_country` = "Vatican City State";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  504 WHERE `iso_country` = "Honduras";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  852 WHERE `iso_country` = "Hong Kong";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  36 WHERE `iso_country` = "Hungary";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  354 WHERE `iso_country` = "Iceland";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  91 WHERE `iso_country` = "India";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  62 WHERE `iso_country` = "Indonesia";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  98 WHERE `iso_country` = "Iran, Islamic Republic of";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  964 WHERE `iso_country` = "Iraq";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  353 WHERE `iso_country` = "Ireland";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  972 WHERE `iso_country` = "Israel";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  39 WHERE `iso_country` = "Italy";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  1876 WHERE `iso_country` = "Jamaica";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  81 WHERE `iso_country` = "Japan";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  962 WHERE `iso_country` = "Jordan";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  7 WHERE `iso_country` = "Kazakhstan";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  254 WHERE `iso_country` = "Kenya";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  686 WHERE `iso_country` = "Kiribati";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  850 WHERE `iso_country` = "Korea, Democratic People\'s Republic of";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  82 WHERE `iso_country` = "Korea, Republic of (South)";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  965 WHERE `iso_country` = "Kuwait";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  996 WHERE `iso_country` = "Kyrgyzstan";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  856 WHERE `iso_country` = "Lao People\'s Democratic Republic";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  371 WHERE `iso_country` = "Latvia";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  961 WHERE `iso_country` = "Lebanon";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  266 WHERE `iso_country` = "Lesotho";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  231 WHERE `iso_country` = "Liberia";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  218 WHERE `iso_country` = "Libyan Arab Jamahiriya";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  423 WHERE `iso_country` = "Liechtenstein";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  370 WHERE `iso_country` = "Lithuania";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  352 WHERE `iso_country` = "Luxembourg";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  853 WHERE `iso_country` = "Macau";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  389 WHERE `iso_country` = "Macedonia, the Former Yugoslav Republic of";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  261 WHERE `iso_country` = "Madagascar";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  265 WHERE `iso_country` = "Malawi";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  60 WHERE `iso_country` = "Malaysia";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  960 WHERE `iso_country` = "Maldives";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  223 WHERE `iso_country` = "Mali";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  356 WHERE `iso_country` = "Malta";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  692 WHERE `iso_country` = "Marshall Islands";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  596 WHERE `iso_country` = "Martinique";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  222 WHERE `iso_country` = "Mauritania";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  230 WHERE `iso_country` = "Mauritius";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  269 WHERE `iso_country` = "Mayotte";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  52 WHERE `iso_country` = "Mexico";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  691 WHERE `iso_country` = "Micronesia";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  373 WHERE `iso_country` = "Moldova";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  377 WHERE `iso_country` = "Monaco";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  976 WHERE `iso_country` = "Mongolia";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  1664 WHERE `iso_country` = "Montserrat";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  382 WHERE `iso_country` = "Montenegro";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  212 WHERE `iso_country` = "Morocco";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  258 WHERE `iso_country` = "Mozambique";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  95 WHERE `iso_country` = "Myanmar";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  264 WHERE `iso_country` = "Namibia";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  674 WHERE `iso_country` = "Nauru";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  977 WHERE `iso_country` = "Nepal";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  31 WHERE `iso_country` = "Netherlands";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  599 WHERE `iso_country` = "Netherlands Antilles";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  687 WHERE `iso_country` = "New Caledonia";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  64 WHERE `iso_country` = "New Zealand";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  505 WHERE `iso_country` = "Nicaragua";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  227 WHERE `iso_country` = "Niger";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  234 WHERE `iso_country` = "Nigeria";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  683 WHERE `iso_country` = "Niue";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  672 WHERE `iso_country` = "Norfolk Island";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  1670 WHERE `iso_country` = "Northern Mariana Islands";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  47 WHERE `iso_country` = "Norway";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  968 WHERE `iso_country` = "Oman";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  92 WHERE `iso_country` = "Pakistan";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  680 WHERE `iso_country` = "Palau";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  970 WHERE `iso_country` = "Palestinian Territory, Occupied";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  507 WHERE `iso_country` = "Panama";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  675 WHERE `iso_country` = "Papua New Guinea";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  595 WHERE `iso_country` = "Paraguay";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  51 WHERE `iso_country` = "Peru";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  63 WHERE `iso_country` = "Philippines";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  0 WHERE `iso_country` = "Pitcairn";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  48 WHERE `iso_country` = "Poland";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  351 WHERE `iso_country` = "Portugal";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  1787 WHERE `iso_country` = "Puerto Rico";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  974 WHERE `iso_country` = "Qatar";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  262 WHERE `iso_country` = "Reunion";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  40 WHERE `iso_country` = "Romania";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  70 WHERE `iso_country` = "Russian Federation";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  250 WHERE `iso_country` = "Rwanda";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  290 WHERE `iso_country` = "Saint Helena";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  1869 WHERE `iso_country` = "Saint Kitts and Nevis";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  1758 WHERE `iso_country` = "Saint Lucia";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  508 WHERE `iso_country` = "Saint Pierre and Miquelon";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  1784 WHERE `iso_country` = "Saint Vincent and the Grenadines";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  684 WHERE `iso_country` = "Samoa";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  378 WHERE `iso_country` = "San Marino";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  239 WHERE `iso_country` = "Sao Tome and Principe";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  966 WHERE `iso_country` = "Saudi Arabia";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  221 WHERE `iso_country` = "Senegal";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  381 WHERE `iso_country` = "Serbia and Montenegro";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  248 WHERE `iso_country` = "Seychelles";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  232 WHERE `iso_country` = "Sierra Leone";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  65 WHERE `iso_country` = "Singapore";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  421 WHERE `iso_country` = "Slovakia";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  386 WHERE `iso_country` = "Slovenia";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  677 WHERE `iso_country` = "Solomon Islands";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  252 WHERE `iso_country` = "Somalia";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  27 WHERE `iso_country` = "South Africa";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  0 WHERE `iso_country` = "South Georgia and the South Sandwich Islands";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  34 WHERE `iso_country` = "Spain";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  94 WHERE `iso_country` = "Sri Lanka";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  249 WHERE `iso_country` = "Sudan";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  597 WHERE `iso_country` = "Suriname";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  47 WHERE `iso_country` = "Svalbard and Jan Mayen Islands";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  268 WHERE `iso_country` = "Swaziland";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  46 WHERE `iso_country` = "Sweden";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  41 WHERE `iso_country` = "Switzerland";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  963 WHERE `iso_country` = "Syrian Arab Republic";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  886 WHERE `iso_country` = "Taiwan";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  992 WHERE `iso_country` = "Tajikistan";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  255 WHERE `iso_country` = "Tanzania, United Republic of";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  66 WHERE `iso_country` = "Thailand";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  670 WHERE `iso_country` = "Timor-Leste";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  228 WHERE `iso_country` = "Togo";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  690 WHERE `iso_country` = "Tokelau";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  676 WHERE `iso_country` = "Tonga";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  1868 WHERE `iso_country` = "Trinidad and Tobago";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  216 WHERE `iso_country` = "Tunisia";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  90 WHERE `iso_country` = "Turkey";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  7370 WHERE `iso_country` = "Turkmenistan";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  1649 WHERE `iso_country` = "Turks and Caicos Islands";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  688 WHERE `iso_country` = "Tuvalu";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  256 WHERE `iso_country` = "Uganda";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  380 WHERE `iso_country` = "Ukraine";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  971 WHERE `iso_country` = "United Arab Emirates";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  44 WHERE `iso_country` = "United Kingdom";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  1 WHERE `iso_country` = "United States";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  1 WHERE `iso_country` = "United States Minor Outlying Islands";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  598 WHERE `iso_country` = "Uruguay";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  998 WHERE `iso_country` = "Uzbekistan";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  678 WHERE `iso_country` = "Vanuatu";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  58 WHERE `iso_country` = "Venezuela";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  84 WHERE `iso_country` = "Viet Nam";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  1284 WHERE `iso_country` = "Virgin Islands, British";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  1340 WHERE `iso_country` = "Virgin Islands, U.s.";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  681 WHERE `iso_country` = "Wallis and Futuna Islands";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  212 WHERE `iso_country` = "Western Sahara";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  967 WHERE `iso_country` = "Yemen";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  260 WHERE `iso_country` = "Zambia";');
	BdConn::runSql('UPDATE ip2nationCountries set `phonecode` =  263 WHERE `iso_country` = "Zimbabwe";');

	BdConn::runSql('INSERT INTO labels (`langID`, `type`, `label`, `kind`) VALUES (1, "FORMPHONECODELABEL", "Indicativo","general");');
	BdConn::runSql('INSERT INTO labels (`langID`, `type`, `label`, `kind`) VALUES (3, "FORMPHONECODELABEL", "Phone Code","general");');
	BdConn::runSql('INSERT INTO labels (`langID`, `type`, `label`, `kind`) VALUES (4, "FORMPHONECODELABEL", "Indicativo","general");');

	BdConn::runSql('UPDATE settings SET databaseVersion  = 1.9');
}
if ($databaseVersion < 2) {
	if (!count($bdconn->query("SHOW COLUMNS FROM `ip2nationCountries` LIKE 'phonecode'")->fetchAll())) {
		BdConn::runSql('ALTER TABLE ip2nationCountries ADD phonecode bigint default null;');
	}
	BdConn::runSql('ALTER TABLE contentsInfo ADD COLUMN `payment_link` VARCHAR(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL;');
	BdConn::runSql('UPDATE settings SET databaseVersion  = 2');
}


if (isset($_COOKIE['cart'])) {
	$cart = StoreManager::getFullCart();
}

$settings = SettingsManager::getSettings();

if (isset($_SESSION["userID"])) {

	if (UtilsManager::is_serialized($_SESSION["userID"])) {
		$userVO = unserialize($_SESSION["userID"]);
	} else {
		$userVO = UtilsManager::fixObject($_SESSION["userID"]);
	}

	$activationCode = $userVO->activationCode;
	$userVO = UserManager::currentUser($activationCode);
	$ema::$CURRENTUSER = $userVO;
}
$country = IP2NationManager::getCountry();


$countryCode = IP2NationManager::getCode($country);
$languageByLocale =  LanguageManager::getLanguageByLocale($locale);



$language = $languageByLocale;
if (!$languageByLocale->ID) {
	$language = LanguageManager::getDefaultLanguage();
}



function getURLLevel($level)
{
	$levels = array();
	if (isset($_GET['id'])) {
		$levels  = preg_split('[/]', strtolower($_GET['id']));

		$selectedLevel = "";
		if (is_numeric($level)) {
			for ($i = 0; $i <= $level; ++$i) {
				if ($i < $level) {
					$selectedLevel .= $levels[$i] . "/";
				} else {
					if (isset($levels[$i])) {
						$selectedLevel .= $levels[$i];
					}
				}
			}
		}
	}
	if ($level >= count($levels)) {
		$selectedLevel = "";
	}
	return $selectedLevel;
}
function getAddress()
{
	/*** check for https ***/
	$protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') ? 'https' : 'http';

	/*** return the full address ***/
	return $protocol . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
}
function returnURLLevels()
{
	$levels = "";
	if (isset($_GET['id'])) {
		$levels  = preg_split('[/]', strtolower($_GET['id']));
	}
	return count($levels);
}


// DEFINE CONTENT ID

if (isset($_GET['id'])) {
	$selectedID = $_GET['id'];
}


// CHECK IF DEEPLINK EXISTS
if (isset($selectedID)) {
	if (!$language->ID) {
		$language = LanguageManager::getDefaultLanguage();
	}
	$content = ContentsManager::getContentByDeeplink($selectedID);

	if (!$content->UID) {
		$tryNewDeeplink = substr($selectedID, 0, strlen($selectedID) - 1);

		$content = ContentsManager::getContentByDeeplink($tryNewDeeplink);

		if (!$content->UID) {
			tryFileDeeplink($selectedID);
			returnn404($selectedID);
		}
		if (!$content->enabled) {
			returnn404($selectedID);
		}
	}

	if (!$content->enabled) {
		if (!isset($_GET['preview'])) {
			returnn404($selectedID);
		}
	}
} else {


	if (isset($_COOKIE['languageID'])) {
		$languageID = $_COOKIE['languageID'];
		$language = LanguageManager::getLanguage($languageID);
	} else {
		if (!$language->ID) {
			$language = LanguageManager::getDefaultLanguage();
		}
		$languageID = $language->ID;
	}
	$content = ContentsManager::getHomeContent($languageID);
	$menu = MenuManager::getMenu($languageID);
	$labels = LabelsManager::getLabels($languageID);

	if ($content->automaticSelectChild) {
		$content  = ContentsManager::getDeeplink($content->UID, $content->automaticSelectChild, $languageID);
		header("location:" . $content->deeplink);
	}

	/*
$languageID = $languageByIP->ID;

	$content = ContentsManager::getHomeContent ($languageID);
	$menu = MenuManager::getMenu($languageID);
	$labels = LabelsManager::getLabels($languageID);
	if($content->automaticSelectChild)
	{
		$content  = ContentsManager::getAutomaticSelectedChild($content->UID, $content->automaticSelectChild, $languageID);
		header("location:".$content->deeplink);
	}
*/
}

if ($content->template) {

	// DEFINES LANGUAGE ID
	if (!isset($languageID)) {
		$languageID = $content->langID;
		$language = LanguageManager::getLanguage($languageID);
		setcookie('languageID', $languageID, time() + 7 * 24 * 60 * 60, "/");
	}

	$menu = MenuManager::getMenu($languageID);
	$labels = LabelsManager::getLabels($languageID);



	// CHECK IF CONTENT IS LOCKED
	if (!$content->locked) {
		// CHECK IF TEMPLATE FILE EXISTS, IF NOT USES DEFAULT.template
		if (file_exists(TEMPLATESPATH . $content->template)) {
			loadFrameworkTemplate($content->template);
		} else {

			loadFrameworkTemplate("default.php");
		}
	} else {

		if (isset($_SESSION["userID"])) {
			if (is_string($_SESSION["userID"])) {
				$userVO = unserialize($_SESSION["userID"]);
			} else {
				$userVO = $_SESSION["userID"];
			}

			if (AuthorizationManager::hasUserAccessToContent($userVO->ID, $content->UID)) {

				if (file_exists(TEMPLATESPATH . $content->template)) {
					loadFrameworkTemplate($content->template);
				} else {

					loadFrameworkTemplate("default.php");
				}
			} else {
				$parentUID = ContentsManager::getParentUID($content->UID);


				if ($parentUID) {
					// die(var_dump($parentUID));
					if (AuthorizationManager::hasUserAccessToContent($userVO->ID, $parentUID)) {
						if (file_exists(TEMPLATESPATH . $content->template)) {
							loadFrameworkTemplate($content->template);
							exit;
						}
					}
				}


				loadFrameworkTemplate("default.php");
			}
		} else {

			if ($content->template == "default.php") {
				header("location:" . $settings->siteURL);
			} else {
				loadFrameworkTemplate(TEMPLATE_NOLOGIN_FALLBACK);
			}
		}
	}
} else {

	loadFrameworkTemplate("default.php");
}

function defineGlobalSettings()
{
	global $languageID;
	global $language;
	global $menu;
	global $labels;

	if (isset($_COOKIE['languageID'])) {
		$languageID = $_COOKIE['languageID'];
		$language = LanguageManager::getLanguage($languageID);
	} else {
		$language = LanguageManager::getDefaultLanguage();
		$languageID = $language->ID;
	}
	//$content = ContentsManager::getHomeContent ($languageID);
	$menu = MenuManager::getMenu($languageID);
	$labels = LabelsManager::getLabels($languageID);
}

function tryFileDeeplink($selectedID)
{
	defineGlobalSettings();
	$file = FilesManager::getFileByDeeplink($selectedID);
	if (!$file->ID) {
		returnn404($selectedID);
	}
	global $content;
	$content->title = $file->label;
	$content->lead = $file->description;
	$content->deeplink = $file->deeplink;
	$content->files->default = $file;
	loadFrameworkTemplate(TEMPLATE_DEFAULT_FILE);
	exit();
}

function returnn404($selectedID = NULL)
{
	$oldUrl = tryRedirect($selectedID);
	if (isset($oldUrl->url)) {
		if ($oldUrl->url) {
			// die(var_dump($oldUrl->new_url));
			$location = "Location: /";

			$parsed = parse_url($oldUrl->new_url);

			if (!empty($parsed['scheme'])) {
				$location = "Location: ";
			}

			header($location . $oldUrl->new_url, true, $oldUrl->redirect_code);
			exit();
		}
	} else {
		header('HTTP/1.0 404 Not Found');
		loadFrameworkTemplate("404.php");
		exit();
	}
}

function tryRedirect($url)
{
	return ContentsManager::getRedirectMatch($url);
}

function loadFrameworkTemplate($template)
{
	global $language;
	setlocale(LC_ALL, $language->translateCode);

	global $settings;
	$mustacheSettings = array('settings' => LabelsManager::mustacheConvertObjects($settings));

	global $languageID;

	global $menu;
	$mustacheMenu = array();
	if (is_array($menu)) {
		$mustacheMenu = array('menu' => LabelsManager::mustacheConvertObjects($menu, "UID"));
	}

	global $content;

	if (defined("FOUNDATION_INTERCHANGE_IMAGES")) {
		if (FOUNDATION_INTERCHANGE_IMAGES) {
			if ($content->lead != "") {
				$dom = new DOMDocument();
				// CLEAN WARNINGS !?!?
				libxml_use_internal_errors(true);
				$dom->loadHTML(mb_convert_encoding($content->lead, 'HTML-ENTITIES', 'UTF-8'), LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
				// CLEAN WARNINGS !?!?
				libxml_clear_errors();
				foreach ($dom->getElementsByTagName('img') as $image) {
					$siteUrl = SettingsManager::getSiteURL();
					$src = str_replace($siteUrl, "", $image->getAttribute('src'));
					$image->setAttribute('src', $src);

					$old_src = $image->getAttribute('src');
					// $image->setAttribute('src', $new_src);
					$image->setAttribute('data-src', $old_src);
					$responsive = FilesManager::getResponsiveImageFile($old_src);
					if (count($responsive)) {
						$interchange = "";
						for ($i = 0; $i < count($responsive); $i++) {
							$interchange .= "[" . $responsive[$i]->file . ", " . $responsive[$i]->size . "]";
							if ($i < count($responsive) - 1) {

								$interchange .= ",";
							}
						}
						$image->setAttribute('data-interchange', $interchange);
					}
				}

				$content->lead = $dom->saveHTML();
			}
			if ($content->body != "") {
				$dom = new DOMDocument();
				// CLEAN WARNINGS !?!?
				libxml_use_internal_errors(true);
				$dom->loadHTML(mb_convert_encoding($content->body, 'HTML-ENTITIES', 'UTF-8'), LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
				// CLEAN WARNINGS !?!?
				libxml_clear_errors();
				foreach ($dom->getElementsByTagName('img') as $image) {

					$old_src = $image->getAttribute('src');
					// $image->setAttribute('src', $new_src);
					$image->setAttribute('data-src', $old_src);
					$responsive = FilesManager::getResponsiveImageFile($old_src);
					if (count($responsive)) {
						$interchange = "";
						for ($i = 0; $i < count($responsive); $i++) {
							$interchange .= "[" . $responsive[$i]->file . ", " . $responsive[$i]->size . "]";
							if ($i < count($responsive) - 1) {

								$interchange .= ",";
							}
						}
						$image->setAttribute('data-interchange', $interchange);
					}
				}

				$content->body = $dom->saveHTML();
			}
		}
	}
	$mustacheContent = array('content' => LabelsManager::mustacheConvertObjects($content));

	global $labels;
	$mustacheLabels = array();
	if (is_array($labels)) {
		$mustacheLabels = array('labels' => LabelsManager::mustacheConvertObjects($labels, "type"));
	}

	global $activationCode;
	global $selectedID;

	global $language;
	$mustacheLanguage = array('language' => LabelsManager::mustacheConvertObjects($language));

	global $userVO;
	$mustacheUserVO = array();
	if (is_array($userVO)) {
		$mustacheUserVO = array('userVO' => LabelsManager::mustacheConvertObjects($userVO));
	}


	global $cart;
	$mustacheCart = array();
	if (is_array($cart)) {
		$mustacheCart = array('cart' => LabelsManager::mustacheConvertObjects($cart));
	}


	Mustache_Autoloader::register();
	$mustache = new Mustache_Engine();
	global $allConvertedData;
	$allConvertedData = array_merge($mustacheSettings, $mustacheMenu, $mustacheContent, $mustacheLabels, $mustacheUserVO, $mustacheCart, $mustacheLanguage);

	$templateProcessed = getRenderedHTML(TEMPLATESPATH . $template);
	//var_dump($allConvertedData);
	echo $mustache->render($templateProcessed, $allConvertedData);
}

function getRenderedHTML($path)
{
	global $language;
	setlocale(LC_ALL, $language->translateCode);
	global $settings;
	global $languageID;
	global $menu;
	global $content;
	global $labels;
	global $activationCode;
	global $selectedID;
	global $language;
	global $userVO;
	global $cart;
	global $country;
	global $ema;
	global $allConvertedData;
	ob_start();
	include($path);
	$var = ob_get_contents();
	ob_end_clean();
	return $var;
}

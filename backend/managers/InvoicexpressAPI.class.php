<?php
class InvoiceXpressAPI {
    private $apiKey;
    private $accountName;

    public function __construct($apiKey, $accountName) {
        $this->apiKey = $apiKey;
        $this->accountName = $accountName;
    }

    // Create Invoice in InvoiceXpress
    public function createInvoice($clientName, $clientEmail, $amount, $description) {
        $invoiceData = [
            "invoice" => [
                "date" => date('Y-m-d'),
                "due_date" => date('Y-m-d', strtotime('+30 days')),
                "client" => [
                    "name" => $clientName,
                    "email" => $clientEmail
                ],
                "items" => [
                    [
                        "name" => $description,
                        "quantity" => 1,
                        "unit_price" => $amount / 100, // Convert back to decimal
                        "tax" => 23 // VAT rate, adjust as needed
                    ]
                ],
                "status" => "final"
            ]
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://{$this->accountName}.app.invoicexpress.com/invoices.json");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($invoiceData));
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json',
            'Accept: application/json',
            "Authorization: Bearer {$this->apiKey}"
        ]);

        $response = curl_exec($ch);
        curl_close($ch);

        return json_decode($response, true);
    }
}
<?php

class StoreManager
{
    public static function createCart($country = null)
    {
        date_default_timezone_set("Europe/London");
        $creationDate = date('y-m-d H:i:s');
        if (!isset($_SESSION)) {
            session_start();
        }
        $userID = 0;
        if (isset($_SESSION["userID"])) {
            $userVO = new UserVO((array) $_SESSION["userID"]);
            $userID = $userVO->ID;
        }
        if (!isset($country)) {
            $country = IP2NationManager::getCountry();
        }
        if (!isset($_COOKIE['cart'])) {



            BdConn::runSQL("INSERT INTO storeCarts (userID, creationDate, country) VALUES ('" . $userID . "','" . $creationDate . "', '" . $country['country'] . "')");

            $connectPDO = BdConn::connect();
            $cartID = $connectPDO->lastInsertId();

            setcookie("cart", $cartID, time() + 60 * 60 * 24 * 365, "/");
            $_COOKIE['cart'] = $cartID;
            LogManager::logAction($userID, 6, null, null, $cartID);
        } else {
            $cartID = $_COOKIE['cart'];
        }

        return $cartID;
    }

    public static function getFullCart()
    {

        if (isset($_COOKIE['languageID'])) {
            $languageID = $_COOKIE['languageID'];
        } else {
            $languageID = LanguageManager::getDefaultLanguage()->ID;
        }
        $language = LanguageManager::getLanguage($languageID);

        $cartVO = new StoreCartVO();

        if (isset($_COOKIE['cart'])) {
            $cartID = $_COOKIE['cart'];

            $resultset = BdConn::query("SELECT * FROM storeCarts WHERE ID =" . $cartID);
            $row = BdConn::single($resultset);

            $cartVO = new StoreCartVO((array) $row);
            $cartVO->currencySymbol = $language->currencySymbol;
            $cartVO->ID = $cartID;

            /*$products = BdConn::runSQL('SELECT
            contentsInfo.contentsUID as UID,
            storeCartsProducts.quantity
            FROM storeCartsProducts INNER JOIN contentsInfo ON storeCartsProducts.contentsUID = contentsInfo.contentsUID
            INNER JOIN storeCarts ON storeCartsProducts.storeCartsID = storeCarts.ID WHERE storeCarts.ID = '.$cartID.' AND contentsInfo.langID = '.$languageID); */

            $products = BdConn::runSQL('SELECT * from storeCartsProducts WHERE storeCartsID = ' . $cartID);
            $totalPrice = 0;
            $vatTotalValue = 0;
            $totalItens = 0;
            $totalWeight = 0;
            $productsTotalPriceWhitoutVat = 0;

            $cartVO->vatRate = new VatRateVO();
            $cartVO->products = array();
            foreach ($products as $row) {

                if ($row['contentsUID'] != 0) {

                    $contentVO = ContentsManager::getContent($row['contentsUID'], $languageID);

                    $parentContent = ContentsManager::getParentContent($contentVO->UID, $languageID);

                    if (!count($contentVO->files->imageFiles)) {
                        //$files = FilesManager::getContentFiles($parentContentUID, $languageID);
                        $contentVO->files = $parentContent->files;
                    }
                    if (!$contentVO->title) {
                        $contentVO->title = $parentContent->title;
                    }
                    if (self::getVatRate($contentVO->storeVatRateID)->vat > $cartVO->vatRate->vat) {
                        $cartVO->vatRate = self::getVatRate($contentVO->storeVatRateID);
                    }
                } else {
                    $contentVO = new ContentVO();

                    $contentVO->title = $row['customTitle'];
                    //$contentVO->vatPrice = (float) $row['customPrice'];

                    if (self::getVatRate($contentVO->storeVatRateID)->vat > $cartVO->vatRate->vat) {
                        $cartVO->vatRate = self::getVatRate($contentVO->storeVatRateID);
                    }
                    $file = new FileVO();
                    $file->file = $row['customImage'];
                    $contentVO->files->imageFiles[0] = $file;
                    $contentVO->notes = $row['notes'];
                }

                $contentVO->storeCartProductID = $row['ID'];
                $contentVO->quantity = (float) $row['quantity'];
                $cartVO->vatRateValue = (float) number_format($cartVO->vatRate->vat, 2);

                $totalPrice += $contentVO->vatPrice * $contentVO->quantity;
                $productsTotalPriceWhitoutVat += $contentVO->price * $contentVO->quantity;

                $totalWeight += $contentVO->weight * $contentVO->quantity;
                $totalItens += $contentVO->quantity;


                $vatTotalValue += ($contentVO->vatPrice * $contentVO->quantity) - ($contentVO->price * $contentVO->quantity);
                $cartVO->products[] = $contentVO;
            }
            $cartVO->productsTotalPriceWhitoutVat = (float) $productsTotalPriceWhitoutVat;
            $cartVO->totalItens = (float) $totalItens;
            $cartVO->vatTotalValue = (float) $vatTotalValue + $cartVO->shippingFeesVat;
            $cartVO->totalWeight = (float) $totalWeight;
            $cartVO->totalWeightInKilograms = (float) $totalWeight / 1000;
            $cartVO->shippingFees = (float) self::calculateWeight($cartVO->country, $totalWeight, $totalPrice);
            if (isset($cartVO->shippingCustomValue)) {
                $cartVO->shippingFees = $cartVO->shippingCustomValue;
            }

            $cartVO->shippingFeesVat = (float) $cartVO->shippingFees * ($cartVO->vatRate->vat / 100);
            $cartVO->productsTotalPrice = (float) $totalPrice;
            $cartVO->totalPrice = (float) $totalPrice + (float) $cartVO->shippingFees + (float) $cartVO->shippingFeesVat;
            $cartVO->shippingFeesWhitoutVat = (float) $cartVO->shippingFees; //(float)self::getVatValueFromPrice($cartVO->shippingFees, $cartVO->vatRate->vat);
            $cartVO->totalPriceWhitoutVat = (float)number_format($cartVO->productsTotalPriceWhitoutVat + $cartVO->shippingFeesWhitoutVat, 2);
            $cartVO->vatTotalValueWithFeeVat = (float) number_format($cartVO->vatTotalValue + $cartVO->shippingFeesVat, 2);
        }
        if (!is_array($cartVO->products) || !count($cartVO->products)) {
            self::clearCart();
        }
        return $cartVO;
    }

    public static function addProduct($productID, $productQuantity = null)
    {
        $cartID = self::currentCart();
        if ($cartID) {
            if (isset($productQuantity)) {
                $quantity = $productQuantity;
            } else {
                $quantity = 1;
            }
            $userID = 0;
            if (isset($_SESSION["userID"])) {
                $userVO = new UserVO((array) $_SESSION["userID"]);
                $userID = $userVO->ID;
            }
            LogManager::logAction($userID, 14, null, null, $cartID . "," . $productID . "," . $productQuantity);
            if (!self::checkIfProductExists($productID)) {

                $sql = "INSERT INTO storeCartsProducts (storeCartsID, contentsUID, quantity) VALUES ('" . $cartID . "','" . $productID . "', $quantity)";

                $resultset = BdConn::runSQL($sql);

                if ($resultset) {
                    return self::getFullCart();
                }
            } else {
                $quantity = self::checkIfProductExists($productID);
                if (isset($productQuantity)) {
                    $quantity += $productQuantity;
                } else {
                    $quantity++;
                }

                return self::updateProductQuantityByProductID($productID, $quantity);
            }
        }
    }

    public static function addCustomProduct($productVO)
    {
        $cartID = self::currentCart();
        if ($cartID) {
            $sql = "INSERT INTO storeCartsProducts (storeCartsID, customImage, customPrice, notes, customTitle) VALUES ('" . $cartID . "','" . $productVO->optional1 . "', '" . $productVO->price . "', '" . $productVO->notes . "', '" . $productVO->title . "')";

            $resultset = BdConn::runSQL($sql);

            if ($resultset) {
                return true;
            }
        }
    }

    public static function checkIfProductExists($productID)
    {

        $cartID = self::currentCart();
        $sql = "SELECT * FROM storeCartsProducts WHERE storeCartsID= '" . $cartID . "' AND contentsUID = '" . $productID . "' ";

        $resultset = BdConn::runSQL($sql);
        $response = array();
        foreach ($resultset as $row) {
            //var_dump($row);
            $response[] = $row['quantity'];
        }
        if (!count($response)) {
            return false;
        }
        return $response[0];
    }


    public static function deleteProduct($id)
    {

        $cartID = self::currentCart();

        $sql = "DELETE FROM storeCartsProducts WHERE contentsUID= " . $id . " AND storeCartsID = " . $cartID;

        $resultset = BdConn::query($sql);

        return self::getFullCart();
    }

    public static function updateProductQuantityByProductID($id, $quantity)
    {
        $cartID = self::currentCart();

        if (self::checkIfProductExists($id) == 0) {
            self::addProduct($id);
            return self::getFullCart();
        }

        if ($quantity) {
            $sql = "UPDATE storeCartsProducts SET quantity='" . $quantity . "'  WHERE storeCartsID= '" . $cartID . "' AND contentsUID = '" . $id . "' ";
        } else if ($quantity == 0) {
            $sql = "DELETE FROM storeCartsProducts WHERE  storeCartsID= '" . $cartID . "' AND contentsUID = '" . $id . "' ";
        }

        $resultset = BdConn::runSQL($sql);

        if (!self::checkIfCartHasProducts()) {
            return self::clearCart();
        }

        return self::getFullCart();
    }

    public static function updateProductQuantity($id, $quantity)
    {
        $cartID = self::currentCart();
        if ($quantity) {
            $sql = "UPDATE storeCartsProducts SET quantity=" . $quantity . " WHERE storeCartsID= " . $cartID . " AND contentsUID = " . $id;
        } else if ($quantity == 0) {
            $sql = "DELETE FROM storeCartsProducts WHERE contentsUID= " . $id . " AND storeCartsID= " . $cartID;
        }

        $resultset = BdConn::runSQL($sql);

        return self::getFullCart();
    }

    public static function currentCart()
    {
        $cartID = (isset($_COOKIE['cart'])) ? $_COOKIE['cart'] : self::createCart();
        return $cartID;
    }

    public static function clearCart()
    {
        if (isset($_COOKIE['cart'])) {
            BdConn::runSQL("DELETE FROM storeCarts WHERE ID=" . $_COOKIE['cart']);
            BdConn::runSQL("DELETE FROM storeCartsProducts WHERE storeCartsID=" . $_COOKIE['cart']);
        }
        if (!isset($_SESSION)) session_start();
        $userID = "0";
        if (isset($_SESSION["userID"])) {
            $userVO = new UserVO((array) $_SESSION["userID"]);
            $userID = $userVO->ID;
        }

        if (isset($_COOKIE['cart'])) {
            unset($_COOKIE['cart']);
            LogManager::logAction($userID, 7);
            return setcookie('cart', '', time() - 3600, "/");
        }
    }

    public static function checkIfCartHasProducts()
    {
        $cartID = self::currentCart();
        $sql = "SELECT * FROM storeCartsProducts WHERE storeCartsID=" . $cartID;
        $resultset = BdConn::runSQL($sql);
        $response = array();
        foreach ($resultset as $row) {
            $response[] = $row['contentsUID'];
        }
        if (!count($response)) {
            return false;
        }
        return true;
    }

    public static function updateShippingCustomValue($value)
    {
        if (isset($value)) {
            $cart = StoreManager::getFullCart();

            $resultset = BdConn::runSQL("UPDATE storeCarts SET shippingCustomValue = " . $value . ", editDate = NOW() WHERE ID = " . $cart->ID);
            return $resultset;
        }
        return 0;
    }
    public static function updateShippingCountry($country)
    {
        $cart = StoreManager::getFullCart();

        $cartVO = $cart;
        $cartVO->country = $country;

        $resultset = BdConn::runSQL("UPDATE storeCarts SET country = '" . $country . "' WHERE ID = " . $cartVO->ID);
        return $resultset;
    }

    public static function calculateVatRate($price, $vatRateID)
    {

        $sql = "SELECT vat FROM storeVatRates WHERE ID =" . $vatRateID;

        if (is_numeric($vatRateID) && is_numeric($price)) {
            $resultset = BdConn::query($sql);

            $row = BdConn::single($resultset);
            if (!$row) {
                return null; // Prevent accessing properties of a non-array
            }
            $vatRate = new VatRateVO();
            $vatRate->vat = (float) $row['vat'];
            $priceWithVat = $price * (($vatRate->vat + 100) / 100);

            //return $priceWithVat;
            if (number_format((float)$priceWithVat, 2, '.', '')) {
                return number_format((float)$priceWithVat, 2, '.', '');
            } else {
                return 0;
            }
        }
        return 0;
    }

    public static function calculateWeight($destinyCountry, $weight, $totalCost = NULL)
    {
        $maxCostRule = "";
        if (isset($totalCost)) {
            $maxCostRule = " ";
            // CHECK IF HAS RULE WITH MAXCOST
            $checkSQL = "SELECT count(storeShippingCountry.ip2nationCountry) as count
FROM storeShippingCountry INNER JOIN storeShippingRulesToCountries ON storeShippingCountry.ID = storeShippingRulesToCountries.shippingCountryID
	 INNER JOIN storeShippingRules ON storeShippingRules.ID = storeShippingRulesToCountries.shippingRulesID
WHERE storeShippingCountry.ip2nationCountry = '$destinyCountry' AND storeShippingRules.toWeight = 0 AND storeShippingRules.fromWeight = 0 AND storeShippingCountry.ID = storeShippingRulesToCountries.shippingCountryID";

            BdConn::query($checkSQL);
            $checkRow = BdConn::single();

            //return $row['count'];
            if ($checkRow['count'] >= 1) {
                $sql = "SELECT storeShippingRules.price FROM storeShippingCountry INNER JOIN storeShippingRulesToCountries ON storeShippingCountry.ID = storeShippingRulesToCountries.shippingCountryID INNER JOIN storeShippingRules ON storeShippingRulesToCountries.shippingRulesID = storeShippingRules.ID WHERE storeShippingCountry.ip2nationCountry = '$destinyCountry' AND $totalCost <= orderMaxCost ORDER BY storeShippingRules.price ASC LIMIT 1";
            } else {
                $sql = "SELECT storeShippingRules.price FROM storeShippingCountry INNER JOIN storeShippingRulesToCountries ON storeShippingCountry.ID = storeShippingRulesToCountries.shippingCountryID INNER JOIN storeShippingRules ON storeShippingRulesToCountries.shippingRulesID = storeShippingRules.ID WHERE storeShippingCountry.ip2nationCountry = '$destinyCountry' AND $weight between storeShippingRules.fromWeight and storeShippingRules.toWeight ORDER BY storeShippingRules.price ASC LIMIT 1";
            }
        } else {
            $sql = "SELECT storeShippingRules.price FROM storeShippingCountry INNER JOIN storeShippingRulesToCountries ON storeShippingCountry.ID = storeShippingRulesToCountries.shippingCountryID INNER JOIN storeShippingRules ON storeShippingRulesToCountries.shippingRulesID = storeShippingRules.ID WHERE storeShippingCountry.ip2nationCountry = '$destinyCountry' AND $weight between storeShippingRules.fromWeight and storeShippingRules.toWeight ORDER BY storeShippingRules.price ASC LIMIT 1";
        }


        BdConn::query($sql);
        $row = BdConn::single();
        //$row = BdConn::next($resultset);

        //self::updateShippingCountry($destinyCountry);
        if (isset($row)) {
            if (!$row['price']) {
                /*
                $sql='SELECT storeShippingRules.price
                FROM storeShippingRules 		WHERE '.$weight.' between storeShippingRules.fromWeight and storeShippingRules.toWeight ORDER BY storeShippingRules.price DESC LIMIT 1';
                $resultset=BdConn::runSQL($sql);
                $row = BdConn::next($resultset);
                */


                return 0;
            }

            return $row['price'];
        }
        return 0;
    }


    /**
     * updateUserID
     *
     * @param userID number
     * @return resulset
     */
    public static function updateUserID($userID)
    {

        $cartID = self::currentCart();
        $sql = "UPDATE storeCarts SET userID='" . $userID . "' WHERE ID= '" . $cartID . "'";


        $resultset = BdConn::runSQL($sql, false);

        return $resultset;
    }

    /**
     * Set Order
     *
     * @param userVO , @param cartID number
     * @return boolean
     */
    public static function setOrder($userVO, $allowDuplicateEmail = false, $duploOptin = false)
    {
        $userVO = (array)$userVO;
        if (!isset($_SESSION)) session_start();
        $joinMessage = "";
        $state = true;
        if (isset($_SESSION["userID"])) {
            $userID = unserialize($_SESSION["userID"]);
            $userVO['ID'] = $userID->ID;

            //BdConn::set($userVO, 'usersWebsite');
            UserManager::updateUserOnOrder($userVO);
        } else {
            //$userVO['ID'] = $userID->ID;

            $userVOIn = new UserVO($userVO);

            if (UserManager::isEmailIn($userVO['email']) && !$allowDuplicateEmail) {

                return LabelsManager::getLabel('REGISTERALREADYREGISTERED', $_COOKIE['languageID']);
                die();
            } else {
                $registerReturn = UserManager::registerUser($userVOIn, $_COOKIE['languageID'], $duploOptin, true, 0, $allowDuplicateEmail);

                if ($registerResult->kind = "success") {
                    $userVO['ID'] = $registerReturn->data;

                    $joinMessage = $registerReturn->label;
                    self::updateUserID($userVO['ID']);
                } else {
                    $state = false;
                    return $registerReturn;
                }
            }
        }
        if ($state) {
            // SETS ORDER FROM CART AND CLEANS CART
            if (!$userVO['shippingContactFirstName']) {
                $userVO['shippingContactFirstName'] = $userVO['firstName'];
            }
            if (!$userVO['shippingContactLastName']) {
                $userVO['shippingContactLastName'] = $userVO['lastName'];
            }
            if (!$userVO['shippingContactMobile']) {
                $userVO['shippingContactMobile'] = $userVO['phone'];
            }
            if (!$userVO['shippingAddress']) {
                $userVO['shippingAddress'] = $userVO['address'];
            }
            if (!$userVO['shippingZipcodeZone']) {
                $userVO['shippingZipcodeZone'] = $userVO['zipcodeZone'];
            }
            if (!$userVO['shippingAlternativeZipcode']) {
                $userVO['shippingAlternativeZipcode'] = $userVO['alternativeZipcode'];
            }
            if (!$userVO['shippingCountry']) {
                $userVO['shippingCountry'] = $userVO['country'];
            }

            if (!$userVO['invoiceFirstName']) {
                $userVO['invoiceFirstName'] = $userVO['firstName'];
            }
            if (!$userVO['invoiceLastName']) {
                $userVO['invoiceLastName'] = $userVO['lastName'];
            }
            if (!$userVO['invoiceAddress']) {
                $userVO['invoiceAddress'] = $userVO['address'];
            }
            if (!$userVO['invoiceZipcodeZone']) {
                $userVO['invoiceZipcodeZone'] = $userVO['zipcodeZone'];
            }
            if (!$userVO['invoiceAlternativeZipcode']) {
                $userVO['invoiceAlternativeZipcode'] = $userVO['alternativeZipcode'];
            }
            if (!$userVO['invoiceCountry']) {
                $userVO['invoiceCountry'] = $userVO['country'];
            }
            if (!$userVO['invoiceNif']) {
                $userVO['invoiceNif'] = $userVO['nif'];
            }

            $order = new StoreOrderVO($userVO);
            date_default_timezone_set("Europe/London");
            $subscribe_date = date('y-m-d H:i:s');
            $order->orderDate = $subscribe_date;
            $order->userID = $userVO['ID'];
            $paymentMethod = self::getPaymentMethod($order->paymentMethodID);

            if (isset($paymentMethod->value)) {
                $order->paymentMethodLabel = $paymentMethod->value;
            }


            // 2 means order was made in storeOrdersStates
            $order->stateID = 2;

            $order = (array)$order;

            $cartID = self::currentCart();


            $orderID = BdConn::add($order, 'storeOrders');


            if ($orderID) {

                $products = BdConn::getList('storeCartsProducts', 'storeCartsID = ' . $cartID);

                while ($product = mysql_fetch_object($products)) {
                    $sql = "INSERT INTO storeOrdersProducts (orderID, contentsUID, quantity, customTitle, customPrice, customImage, notes) VALUES ('" . $orderID . "','" . $product->contentsUID . "','" . $product->quantity . "', '" . $product->customTitle . "', '" . $product->customPrice . "','" . $product->customImage . "','" . $product->notes . "')";

                    BdConn::runSQL($sql);
                }

                LogManager::logAction($userVO['ID'], 4);
                $success = LabelsManager::getLabel('STOREORDERMADESUCCESS', $_COOKIE['languageID']);
                if (self::sendOrderEmail($userVO)) {
                    $tmp = $success->label;
                    $tmp .= $joinMessage . "<br/><br/>";
                    $success->label = $tmp;
                    if (self::clearCart()) {
                        return $success;
                    }
                }
            } else {
                LogManager::logAction($userVO['ID'], 5);
                return LabelsManager::getLabel('STOREORDERMADEERROR', $_COOKIE['languageID']);
            }
        }
    }

    /**
     * getPaymentMethod
     *
     * @param $id
     * @return keyValueVO
     */
    public static function getPaymentMethod($ID)
    {
        global $mysqli;
        $query = $mysqli->query("SELECT ID,label as value FROM storePaymentMethods WHERE ID=$ID");
        $paymentMethod = new stdClass();
        if (isset($query->num_rows)) {
            $paymentMethod = $query->fetch_object();
            $paymentMethod->key = "paymentMethod";
        }
        return $paymentMethod;
    }

    /**
     * sendOrderEmail
     *
     * @param $cart
     * @param $user
     * @return boolean
     */
    public static function sendOrderEmail($user)
    {
        $cart = StoreManager::getFullCart();


        if (isset($_COOKIE['languageID'])) {
            $languageID = $_COOKIE['languageID'];
        } else {
            $languageID = LanguageManager::getDefaultLanguage()->ID;
        }


        $settings = SettingsManager::getSettings();
        $countryManagerEmail = self::getDistributionManagerEmail($cart->country);
        $body = $settings->storeOrderEmailTemplate;
        SendEmailManager::sendEmail($countryManagerEmail, LabelsManager::getLabel('STOREORDERMADESUCCESS', $_COOKIE['languageID'])->label, $body, "", "", true, $user['ID']);


        if ($countryManagerEmail != SettingsManager::getOrdersEmail()) {
            SendEmailManager::sendEmail(SettingsManager::getOrdersEmail(), LabelsManager::getLabel('STOREORDERMADESUCCESS', $_COOKIE['languageID'])->label, $body, "", "", true, $user['ID']);
        }


        return SendEmailManager::sendEmail($user['email'], LabelsManager::getLabel('STOREORDERMADESUCCESS', $_COOKIE['languageID'])->label, $body, "", "", true, $user['ID']);
    }

    /**
     * getDistributionManagerEmail
     *
     * @param $country
     * @return string ($email)
     */
    public static function getDistributionManagerEmail($country)
    {

        $sql = "SELECT usersWebsite.email
		FROM usersWebsite INNER JOIN storeDistributionCountriesToContacts ON usersWebsite.ID = storeDistributionCountriesToContacts.usersWebsiteID
		WHERE storeDistributionCountriesToContacts.ip2nationCountries = '" . $country . "'";


        $return = mysql_fetch_array(BdConn::runSQL($sql));

        if (!$return['email']) {
            return SettingsManager::getOrdersEmail();
        }

        return $return['email'];
    }

    /**
     * getAllOrders
     *
     * @param $userID
     * @return array ($orders array)
     */
    public static function getAllOrders($userID = null)
    {
        $ema = EmaBrain::Instance();

        if (AuthorizationManager::hasAuthorization("admin", $ema::$CURRENTUSER)) {


            $userVO = $ema::$CURRENTUSER;
            if (!$userID) {
                $userID = $userVO->ID;
            }



            $res = BdConn::runSql("SELECT * FROM storeOrders WHERE userID = $userID");


            $response = array();
            foreach ($res as $row) {
                $order = new StoreOrderVO($row);
                $response[] = $order;
            }
            return $response;
        } else {
            return false;
        }
    }

    public static function getFullOrder($ID)
    {


        if (session_id() == '') {
            // session has NOT been started
            session_start();
        }

        if (AuthorizationManager::hasAuthorization(__CLASS__, __FUNCTION__, "")) {


            if (isset($_COOKIE['languageID'])) {
                $languageID = $_COOKIE['languageID'];
            } else {
                $languageID = LanguageManager::getDefaultLanguage()->ID;
            }
            $language = LanguageManager::getLanguage($languageID);

            $cartID = $ID;

            $resultset = BdConn::runSQL("SELECT * FROM storeOrders WHERE ID =" . $cartID);
            $row = BdConn::next($resultset);

            $cartVO = new StoreOrderVO($row);
            $cartVO->currencySymbol = $language->currencySymbol;
            $cartVO->user = UserManager::getUser($row['userID']);
            $cartVO->state = self::getOrderState($row['stateID']);


            /*$products = BdConn::runSQL('SELECT
            contentsInfo.contentsUID as UID,
            storeCartsProducts.quantity
            FROM storeCartsProducts INNER JOIN contentsInfo ON storeCartsProducts.contentsUID = contentsInfo.contentsUID
            INNER JOIN storeCarts ON storeCartsProducts.storeCartsID = storeCarts.ID WHERE storeCarts.ID = '.$cartID.' AND contentsInfo.langID = '.$languageID); */

            $products = BdConn::runSQL('SELECT * from storeOrdersProducts WHERE orderID = ' . $cartID);
            $totalPrice = 0;
            $totalItens = 0;
            $totalWeight = 0;


            foreach ($products as $row) {
                if ($row['contentsUID'] != 0 && $row['customImage'] == "") {
                    $contentVO = ContentsManager::getContent($row['contentsUID'], $languageID);

                    $parentContent = ContentsManager::getParentContent($row['contentsUID'], $languageID);
                    if (!count($contentVO->files->imageFiles)) {
                        //$files = FilesManager::getContentFiles($parentContentUID, $languageID);
                        $contentVO->files = $parentContent->files;
                    }
                    if (!$contentVO->title) {
                        $contentVO->title = $parentContent->title;
                    }
                } else {
                    $contentVO = new ContentVO();

                    $contentVO->title = $row['customTitle'];
                    $contentVO->vatPrice = $row['customPrice'];
                    $contentVO->price = $row['customPrice'];
                    $file = new FileVO();
                    $file->file = $row['customImage'];
                    $contentVO->files->imageFiles[0] = $file;
                    $contentVO->notes = $row['notes'];
                }
                $contentVO->storeCartProductID = $row['ID'];
                $contentVO->quantity = $row['quantity'];

                $totalPrice += $contentVO->price * $contentVO->quantity;
                $totalWeight += $contentVO->weight * $contentVO->quantity;
                $totalItens += $contentVO->quantity;

                $cartVO->products[] = $contentVO;
            }

            $cartVO->totalItens = $totalItens;
            $cartVO->totalWeight = $totalWeight;
            $cartVO->shippingFees = self::calculateWeight($cartVO->country, $totalWeight, $totalPrice);
            $cartVO->totalPrice = $totalPrice + $cartVO->shippingFees;

            return $cartVO;
        }
    }

    public static function getOrdersStates()
    {
        if (session_id() == '') {
            // session has NOT been started
            session_start();
        }

        if (AuthorizationManager::hasAuthorization(__CLASS__, __FUNCTION__, "")) {


            $states = BdConn::getList('storeOrdersStates');
            $statesOut;
            while ($state = mysql_fetch_assoc($states)) {
                $statesOut[] = new StoreStateVO($state);
            }
            return $statesOut;
        }
    }

    public static function getOrderState($ID)
    {
        if (session_id() == '') {
            // session has NOT been started
            session_start();
        }

        if (AuthorizationManager::hasAuthorization(__CLASS__, __FUNCTION__, "")) {

            $resultset = BdConn::runSQL("SELECT * FROM storeOrdersStates WHERE ID =" . $ID);
            $row = BdConn::next($resultset);

            $orderState = new StoreStateVO($row);

            return $orderState;
        }
    }

    public static function getVatRate($ID)
    {
        $resultset = BdConn::query("SELECT * FROM storeVatRates WHERE ID =" . $ID);
        $row = BdConn::single($resultset);

        $vatRateVO = new VatRateVO((array) $row);

        return $vatRateVO;
    }

    public static function fixPricesWithoutVat()
    {
        $resultset = BdConn::getList("contentsInfo", "price != 0 AND storeVatRateID != 0");

        $contents = array();
        while ($content = mysql_fetch_assoc($resultset)) {
            $vatRate = self::getVatRate($content['storeVatRateID'])->vat;
            $newPrice = number_format($content['price'] / (($vatRate + 100) / 100), 2);
            BdConn::runSQL("UPDATE contentsInfo set price = " . $newPrice . " WHERE ID = " . $content['ID']);
        }
        return true;
    }

    /**
     * getVatValueFromPrice
     *
     * @param $price
     * @param $vatRate %
     * @return string ($price)
     */
    public static function getVatValueFromPrice($price, $vatRate, $difference = false)
    {
        if (!$difference) {
            return number_format($price / (($vatRate + 100) / 100), 2);
        }
        return number_format(($price - ($price / (($vatRate + 100) / 100))), 2);
    }

    /**
     * setManualOrder
     *
     * @param $userID
     * @return array ($orders array)
     */
    public static function setManualOrder($userID, $productsUID)
    {

        $label = new LabelVO();
        $label->data = [$userID, $productsUID];
        if (BdConn::query("INSERT INTO storeOrders (userID, orderDate) VALUES ($userID, now())")) {
            $id = BdConn::getLastInsertId();

            if (is_array($productsUID)) {
                for ($i = 0; $i < count($productsUID); $i++) {
                    //return "INSERT INTO storeOrdersProducts (orderID, contentsUID) VALUES ($id, $productsUID[$i])";
                    BdConn::query("INSERT INTO storeOrdersProducts (orderID, contentsUID) VALUES ($id, $productsUID[$i])");
                }
                return true;
            } else {
                BdConn::query("INSERT INTO storeOrdersProducts (orderID, contentsUID) VALUES ($id, $productsUID)");
                return true;
            }
        }

        return false;
    }
    /**
     * setManualOrder
     *
     * @param $orderID, productUID
     * @return boolean 
     */
    public static function isProductInOrder($orderID, $productUID)
    {
        $sql = "SELECT * FROM storeOrdersProducts WHERE contentsUID = $productUID AND orderID = $orderID";
        BdConn::query($sql);
        $resultset = BdConn::single();
        return $resultset;
    }

    /**
     * deleteOrder
     *
     * @param $orderID
     * @return boolean 
     */
    public static function deleteOrder($orderID)
    {
        if (!isset($_SESSION)) session_start();
        if (!isset($_SESSION['userID'])) {
            return false;
        }

        $userVO = $_SESSION["userID"];

        if (AuthorizationManager::hasAuthorization("admin", $userVO)) {
            $sql = "DELETE FROM storeOrdersProducts WHERE orderID = $orderID";
            BdConn::query($sql);

            $sql = "DELETE FROM storeOrders WHERE ID = $orderID";
            BdConn::query($sql);
            return true;
        }
    }
}

<?php


class LanguageManager
{

	/*
	*	Author: Jose Luis Gouveia
	*
	*	getLanguages: Returns an Array of LanguageVO with the enabled languages
	*
	*/
	public static function getLanguages()
	{

		$myQuery = "SELECT * from languages WHERE enabled = '1' ORDER BY sortOrder asc";
		$res = BdConn::runSQL($myQuery);
		$response = array();

		foreach ($res as $row) {
			$obj = new LanguageVO($row);

			$sql = "SELECT symbol FROM storeCurrencies WHERE ID ='" . $obj->currencyID . "' ";
			$resultset = BdConn::runSQL($sql);
			$symbol = BdConn::next($resultset);
			$obj->currencySymbol = $symbol;
			$response[] = $obj;
		}
		return $response;
	}

	/*
	*	Author: Jose Luis Gouveia
	*
	*	getLanguagesXML: returns a XML format of all enabled Languages
	*
	*/
	public static function getLanguagesXML()
	{
		$res = mysql_query("SELECT * from languages WHERE enabled = '1' order by id asc");

		$response = "<languages>
";

		while ($row = mysql_fetch_array($res)) {
			$isDefault = "false";
			if ($row['isDefault']) {
				$isDefault = "true";
			}

			$response .= '		<language isDefault="' . $isDefault . '" id="' . $row['ID'] . '" short="' . $row['short'] . '" labels="frontend/inc/getLabelsXML.inc.php?language=' . $row['ID'] . '">' . $row['description'] . '</language>
';
		}
		$response .= "</languages>";
		return $response;
	}

	/*
	*	Author: Jose Luis Gouveia
	*
	*	getLanguageDescription: returns a Description from a Language by a given langID
	*
	*/
	public static function getLanguageDescription($lang)
	{
		if (!$lang) {
			$lang = MAINLANG;
		}
		if (!is_numeric(MAINLANG)) {
			$lang = LanguageManager::getLanguageIDByShort(MAINLANG)->ID;
		}
		$res = mysql_query("SELECT description from languages WHERE ID = '$lang'");

		$response = array();

		while ($row = mysql_fetch_array($res)) {

			return $row['description'];
		}
	}



	/*
	*	Author: Jose Luis Gouveia
	*
	*	getDefaultLanguage: returns the default language
	*
	*/
	public static function getDefaultLanguage()
	{
		BdConn::query("SELECT languages.ID,
	    languages.short,
	    languages.description,
	    languages.currencyID,
	    languages.translateCode,
	    storeCurrencies.symbol as currencySymbol
	    FROM storeCurrencies INNER JOIN languages ON storeCurrencies.ID = languages.currencyID
	    WHERE languages.isDefault = '1' LIMIT 1");

		$query = BdConn::single();

		//$row = mysql_fetch_assoc($query);
		$languageVO = new LanguageVO($query);

		return $languageVO;
	}

	/*
	*	Author: Jose Luis Gouveia
	*
	*	getLanguageIDByShort: returns the VO language by short
	*
	*/
	public static function getLanguageIDByShort($short)
	{
		$short = strtoupper($short);

		$res = BdConn::query("SELECT * from languages WHERE short = '$short' LIMIT 1");

		$row = BdConn::single($res);

		$languageVO = new LanguageVO($row);

		return $languageVO;
	}

	/*
	*	Author: Jose Luis Gouveia
	*
	*	getLanguage: returns the VO language by id
	*
	*/
	public static function getLanguage($id)
	{

		BdConn::query("SELECT languages.ID,
		languages.short,
		languages.description,
		languages.currencyID,
		languages.translateCode,
		storeCurrencies.symbol as currencySymbol
		FROM storeCurrencies INNER JOIN languages ON storeCurrencies.ID = languages.currencyID
		WHERE languages.ID = '" . $id . "' LIMIT 1");


		$res = BdConn::single();

		$languageVO = new LanguageVO($res);

		return $languageVO;
	}

	/*
	*	Author: Jose Luis Gouveia
	*
	*	getLanguage: returns the VO language by short
	*
	*/
	public static function getLanguageByShort($short)
	{

		$res = BdConn::query("SELECT languages.ID,
		languages.short,
		languages.description,
		languages.currencyID,
		languages.translateCode,
		storeCurrencies.symbol as currencySymbol
		FROM storeCurrencies INNER JOIN languages ON storeCurrencies.ID = languages.currencyID
		WHERE languages.short = '" . strtoupper($short) . "' LIMIT 1");


		$res = BdConn::single();

		$languageVO = new LanguageVO($res);

		return $languageVO;
	}
	/*
	*	Author: Jose Luis Gouveia
	*
	*	getLanguageByLocale: returns the VO language by locale
	*
	*/
	public static function getLanguageByLocale($locale)
	{

		$res = BdConn::query("SELECT languages.ID,
		languages.short,
		languages.description,
		languages.currencyID,
		languages.translateCode,
		storeCurrencies.symbol as currencySymbol
		FROM storeCurrencies INNER JOIN languages ON storeCurrencies.ID = languages.currencyID
		WHERE upper(languages.translateCode) = '" . strtoupper($locale) . "' LIMIT 1");


		$res = BdConn::single();

		$languageVO = new LanguageVO($res);

		return $languageVO;
	}
	/*
	*	Author: Jose Luis Gouveia
	*
	*	getLanguage: returns the VO language by country
	*
	*/
	public static function getLanguageByCountry($short)
	{

		if (is_array($short)) {
			$short = $short['code'];
		}

		BdConn::query("SELECT languageID from countriesToLanguages WHERE country = '" . $short . "'");
		$res = BdConn::single();

		$id = $res['languageID'];
		if (!$id) {
			$languageVO = self::getDefaultLanguage();
		} else {
			$languageVO = self::getLanguage($id);
		}

		return $languageVO;
	}


	/*
	*	Author: Jose Luis Gouveia
	*
	*	getAllLanguages: Returns an Array of LanguageVO of all Languages
	*
	*/
	public static function getAllLanguages()
	{

		$myQuery = "SELECT * from languages ORDER BY sortOrder asc";
		$res = mysql_query($myQuery) or die($myQuery . "<br/><br/>" . mysql_error());
		$response = array();

		while ($row = mysql_fetch_array($res)) {
			$obj = new LanguageVO($row);
			$response[] = $obj;
		}
		return $response;
	}

	public static function defineLanguage($ID)
	{
		setcookie("languageID", $ID);
	}

	public static function getActiveLanguage()
	{

		if (!isset($_SESSION)) session_start();

		if (isset($_COOKIE['languageID'])) {
			$languageID = $_COOKIE['languageID'];
			$language = LanguageManager::getLanguage($languageID);
		} else {
			$language = LanguageManager::getDefaultLanguage();
		}

		return $language;
	}
}

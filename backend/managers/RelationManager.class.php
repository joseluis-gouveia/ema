<?php


class RelationManager
{
	/**
	 *
	 * 
	 * @param Int (contentID)
	 * @return Boolean
	 */
	public static function hasContentTagFromUser($contentUID, $userID, $tag)
	{
		$checkUser = UserManager::getUser($userID);
		if (isset($checkUser->ID)) {
			$res = BdConn::runSQL("SELECT * FROM relations WHERE childID = $userID AND parentTable = 'contents' AND childTable = 'usersWebsite' AND parentID = $contentUID AND tag = '$tag'");

			$response = array();
			foreach ($res as $row) {
				return true;
			}

			return false;
		}
	}

	/**
	 *
	 * 
	 * @param Int (contentID)
	 * @param String (tag)
	 * @return Boolean
	 */
	public static function hasAllContentTagInUsers($contentUID, $tag)
	{
		$res = BdConn::runSQL("SELECT * FROM relations WHERE parentTable = 'contents' AND childTable = 'usersWebsite' AND parentID = $contentUID");

		$response = array();
		foreach ($res as $row) {
			if ($row['tag'] != $tag) {
				return false;
			}
		}
		return true;
	}

	/**
	 *
	 * 
	 * @param Int $ID
	 * @return Array
	 */
	public static function getFilesFromUser($userID)
	{
		$checkUser = UserManager::getUser($userID);
		if (isset($checkUser->ID)) {
			$res = BdConn::runSQL("SELECT * FROM relations WHERE parentID = $userID AND parentTable = 'usersWebsite' AND childTable = 'files'");

			$response = array();
			foreach ($res as $row) {
				$vo = FilesManager::getFile($row['childID']);
				$response[] = $vo;
			}

			return $response;
		} else {
			return false;
		}
	}

	/**
	 *
	 * 
	 * @param int (ID)
	 * @return Array
	 */
	public static function getContentsFromUser($userID, $lang = null)
	{

		if (!$lang) {
			$lang = LanguageManager::getDefaultLanguage()->ID;
		} else {
			if (!is_numeric($lang)) {
				$lang = LanguageManager::getLanguageIDByShort($lang)->ID;
			}
		}

		$checkUser = UserManager::getUser($userID);
		if (isset($checkUser->ID)) {
			$res = BdConn::runSQL("SELECT * FROM relations WHERE childID = $userID AND parentTable = 'contents' AND childTable = 'usersWebsite' GROUp BY parentID");

			$response = array();
			foreach ($res as $row) {
				$vo = ContentsManager::getContent($row['parentID']);
				$response[] = $vo;
			}

			return $response;
		} else {
			return false;
		}
	}

	/**
	 *
	 * 
	 * @param int (ID)
	 * @return Array
	 */
	public static function getUsersFromUser($userID)
	{
		$checkUser = UserManager::getUser($userID);

		if (isset($checkUser->ID)) {
			$res = BdConn::runSQL("SELECT childID FROM relations WHERE parentID = $userID AND parentTable = 'usersWebsite' AND childTable = 'usersWebsite'");

			$response = array();
			foreach ($res as $row) {
				$vo = UserManager::getUser($row['childID']);
				$response[] = $vo;
			}

			return $response;
		} else {
			return false;
		}
	}

	/**
	 *
	 * 
	 * @param int (UID)
	 * @return Array
	 */
	public static function getUsersFromContent($UID, $asIdsArray = false)
	{

		$response = array();
		if ($UID) {
			$sql = "SELECT uw.* 
			FROM relations AS r 
			JOIN usersWebsite AS uw ON r.childID = uw.id 
			WHERE r.parentID = $UID 
			  AND r.parentTable = 'contents' 
			  AND r.childTable = 'usersWebsite'
			";
			BdConn::query($sql);
			$res = BdConn::resultset();


			if ($res) {
				foreach ($res as $row) {

					$vo = new UserVO($row);
					if ($vo->ID) {
						if ($asIdsArray) {
							$response[] = $vo->ID;
						} else {
							$response[] = $vo;
						}
					}
				}
			}
		}
		return $response;
	}

	/**
	 *
	 * 
	 * @param int (UID)
	 * @return Array
	 */
	public static function isUserFromContentByEmail($UID, $email_in, $lang = null)
	{
		if (!$lang) {
			$lang = LanguageManager::getDefaultLanguage()->ID;
		} else {
			if (!is_numeric($lang)) {
				$lang = LanguageManager::getLanguageIDByShort($lang)->ID;
			}
		}

		$userManager = new UserManager();
		$response = array();

		if (UserManager::isEmailIn($email_in)) {
			$userVO = $userManager->getUserByEmail($email_in);
			$userID = $userVO->ID;

			if ($UID) {

				$sql = "SELECT * FROM relations WHERE relations.parentID = $UID AND relations.parentTable = 'contents' AND relations.childTable = 'usersWebsite' AND relations.childID = $userID";

				$result = BdConn::query($sql);
				$number = BdConn::single();

				if ($number) {
					return $userManager::recoverPassword($userVO);
				} else {
					return LabelsManager::getLabel("USERNOCONTENT", $lang);
				}
			}
		} else {
			return LabelsManager::getLabel("USERNOUSERERROR", $lang);
		}
	}
	/**
	 *
	 * 
	 * @param int (UID, $userID)
	 * @return Array
	 */
	public static function isUserFromContent($UID, $userID)
	{
		// VERIFICAR SE JÁ EXISTE A RELAÇÃO COM OS MESMOS DADOS
		$sql = "SELECT ID FROM relations WHERE parentTable = 'contents' AND parentID = $UID AND childTable = 'usersWebsite' AND childID = $userID ";

		$resultset = BdConn::query($sql);
		$resultset = BdConn::single();

		if (!$resultset) {
			return false;
		} else {
			return true;
		}
		return false;
	}
	/**
	 *
	 * 
	 * @param int (UID)
	 * @return Array
	 */
	public static function setUserToContent($UID, $userID, $sortOrder = 0)
	{
		// VERIFICAR SE JÁ EXISTE A RELAÇÃO COM OS MESMOS DADOS
		if (!self::isUserFromContent($UID, $userID)) {
			return BdConn::runSQL("INSERT INTO relations (parentTable, parentID, childTable, childID, sortOrder) VALUES ('contents', $UID, 'usersWebsite', $userID, $sortOrder)");
		} else {
			return true;
		}
		return false;
	}

	/**
	 *
	 * 
	 * @param int (UID, userID)
	 * @return boolean
	 */
	public static function deleteUsersFromContent($UID, $userID, $labelIn)
	{
		$ema = EmaBrain::Instance();

		if ($ema::$ALLOW_ADD_CONTENT_NO_SESSION) {
			if (is_array($userID)) {
				for ($i = 0; $i < count($userID); $i++) {
					$id = $userID[$i];
					$res = BdConn::runSQL("DELETE FROM relations WHERE parentID = $UID AND parentTable = 'contents' AND childTable = 'usersWebsite' AND childID=$id");
					if (!$res) {
						return false;
					}
					$label = new LabelVO();
					$label->label = "User removed";
					$label->kind = "success";
					if ($labelIn) {
						$label = LabelsManager::getLabel($labelIn);
					}
					return $label;
				}
			} else {
				$res = BdConn::runSQL("DELETE FROM relations WHERE parentID = $UID AND parentTable = 'contents' AND childTable = 'usersWebsite' AND childID=$userID");
				if (!$res) {
					return false;
				} else {
					$label = new LabelVO();
					$label->label = "User removed";
					$label->kind = "success";
					if ($labelIn) {
						$label = LabelsManager::getLabel($labelIn);
					}
					return $label;
				}
			}
		}
		return false;
	}

	/**
	 *
	 * 
	 * @param int (UID)
	 * @return boolean
	 */
	public static function deleteAllUsersFromContent($UID)
	{
		$ema = EmaBrain::Instance();

		if ($ema::$ALLOW_ADD_CONTENT_NO_SESSION) {

			$res = BdConn::runSQL("DELETE FROM relations WHERE parentID = $UID AND parentTable = 'contents' AND childTable = 'usersWebsite'");
			if (!$res) {
				return false;
			}
		}
		return true;
	}
	/**
	 *
	 * 
	 * @param int (userID)
	 * @return Array
	 */
	public static function getUserRoles($userID)
	{
		// VERIFICAR SE JÁ EXISTE A RELAÇÃO COM OS MESMOS DADOS
		$sql = "SELECT * FROM relations WHERE parentTable = 'usersRoles' AND childID = $userID AND childTable = 'usersWebsite'";
		BdConn::query($sql);
		$res = BdConn::resultset();

		$response = array();
		if ($res) {
			foreach ($res as $row) {
				$response[] = RolesManager::getRole($row['parentID']);
			}
		}

		return $response;
	}

	/**
	 *
	 * Author: Jose Luis Gouveia
	 * updates relation tag 
	 * @param parentTable
	 * @param parentID
	 * @param childTable
	 * @param childID
	 * @param tag
	 * @return boolean
	 */
	public static function updateRelationTag($parentTable, $parentID, $childTable, $childID, $tag)
	{
		if (is_array($childID)) {

			for ($i = 0; $i < count($childID); $i++) {
				$id = $childID[$i];
				$sql = "UPDATE relations SET tag = '$tag' WHERE parentTable = '$parentTable' AND childTable = '$childTable' AND childID = $id AND parentID = $parentID";

				BdConn::query($sql);
			}
			return true;
		} else {
			$sql = "UPDATE relations SET tag = '$tag' WHERE parentTable = '$parentTable' AND childTable = '$childTable' AND childID = $childID AND parentID = $parentID";

			return BdConn::query($sql);
		}
		return false;
	}
}

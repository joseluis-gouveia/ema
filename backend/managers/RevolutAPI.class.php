<?php
class RevolutAPI
{
    private $apiKey;
    private $apiUrl;

    public function __construct($apiKey, $sandbox = false)
    {
        // Use the correct Merchant API URL for sandbox or production
        $this->apiKey = $apiKey;
        $this->apiUrl = $sandbox ? 'https://sandbox-merchant.revolut.com/api/orders' : 'https://merchant.revolut.com/api/orders';
    }

    // Create a new payment order with a redirect URL
    public function createPaymentOrder($amount, $currency, $orderReference, $returnUrl, $metadata)
    {
        $postData = [
            'amount' => $amount, // Amount in the smallest currency unit (e.g., cents)
            'currency' => $currency,
            'metadata' => $metadata,
            'merchant_order_data' => [
                'reference' => $orderReference
            ],
            'redirect_url' => $returnUrl // Redirect URL after payment
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->apiUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($postData));
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Authorization: Bearer ' . $this->apiKey,
            'Content-Type: application/json',
            'Revolut-Api-Version: 2024-09-01'
        ]);

        $response = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $error = curl_error($ch);
        curl_close($ch);

        if ($httpCode === 200 || $httpCode === 201) {
            return json_decode($response, true);
        } else {
            return [
                'error' => true,
                'http_code' => $httpCode,
                'response' => $response,
                'curl_error' => $error
            ];
        }
    }
}

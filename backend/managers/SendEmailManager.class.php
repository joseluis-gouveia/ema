<?php


class SendEmailManager
{

	/**
	 *
	 * Author: Jose Luis Gouveia
	 * send's a email
	 * $nameFrom could be null (default getAdminName)
	 * $emailFrom could be null (default getGlobalEmail)
	 */

	public static function sendEmail($emailTo, $subject, $body, $nameFrom = NULL, $emailFrom = NULL, $useOperationEmail = true, $userID = null, $attachments = null)
	{
		$emailFrom = (!$emailFrom) ? SettingsManager::getGlobalEmail() : $emailFrom;
		$nameFrom = (!$nameFrom) ? SettingsManager::getAdminName() : $nameFrom;


		if ($useOperationEmail) {

			$body = OperationEmailManager::getOperationalEmail($body, $userID);
		}

		$mail = new PHPMailer();



		$smtpOptions['smtp_secure'] = BaseSMTP::SMTP_SECURE;
		$smtpOptions['host'] = BaseSMTP::HOST;
		$smtpOptions['port'] = BaseSMTP::PORT;
		$smtpOptions['username'] = BaseSMTP::USERNAME;
		$smtpOptions['password'] = BaseSMTP::PASSWORD;
		$smtpOptions['smtp_auth'] = BaseSMTP::SMTP_AUTH;
		//return SMTP::HOST;
		if (class_exists('CustomSMTP') && CustomSMTP::HOST != "") {
			$smtpOptions['smtp_secure'] = CustomSMTP::SMTP_SECURE;
			$smtpOptions['host'] = CustomSMTP::HOST;
			$smtpOptions['port'] = CustomSMTP::PORT;
			$smtpOptions['username'] = CustomSMTP::USERNAME;
			$smtpOptions['password'] = CustomSMTP::PASSWORD;
			$smtpOptions['smtp_auth'] = CustomSMTP::SMTP_AUTH;
		}

		if ($smtpOptions['host'] != "") {

			$mail->IsSMTP();
			$mail->SMTPSecure = $smtpOptions['smtp_secure'];
			$mail->Host = $smtpOptions['host'];
			$mail->Port = $smtpOptions['port'];
			$mail->Username = $smtpOptions['username'];
			$mail->Password = $smtpOptions['password'];
			$mail->SMTPAuth = $smtpOptions['smtp_auth'];
		}

		if (is_array($attachments)) {
			for ($i = 0; $i < count($attachments); $i++) {
				$mail->addAttachment($attachments[$i]);
			}
		} else {
			$mail->addAttachment($attachments);
		}
		$pos = strpos($emailFrom, ",");
		$emails = array();
		if ($pos !== false) {
			$emails = explode(",", $emailFrom);
			$emailFrom = $emails[0];
		}
		$mail->From = $emailFrom;
		$mail->CharSet = "UTF-8";

		$mail->FromName = $nameFrom;

		$pos = strpos($emailTo, ",");
		$emails = array();
		if ($pos !== false) {
			$emails = explode(",", $emailTo);
			$emailTo = $emails[0];
		}
		$mail->AddAddress($emailTo);
		if (count($emails)) {
			for ($i = 0; $i < count($emails); $i++) {
				$mail->AddCC($emails[$i]);
			}
		}

		$mail->Subject = $subject;
		$email_body = array();
		$email_body[] = $body;
		$mail->MsgHTML(implode('', $email_body));
		return $mail->Send();
	}
}

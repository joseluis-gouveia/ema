<?php

class AuthorizationManager
{
	public static function authorize($class, $function, $argsArray = null)
	{
		if (!AuthorizationManager::hasAuthorization($class, $function, $argsArray)) {
			throw new Exception("User has no authorization to access $class.$function");
		}
		return true;
	}

	public static function hasAuthorization($role = null, $currentUser = null)
	{

		if (isset($_SESSION["userID"]) && ((!$role && !$currentUser))) {
			return true;
		} else {
			if ($role && $currentUser) {

				for ($i = 0; $i < count($currentUser->roles); $i++) {

					if ($currentUser->roles[$i]->label == $role) {
						return true;
					}
				}
			}
		}
		return false;

		//		$user = UserManager::getUser($_SESSION['userID']);
		//
		//		if ($user->isAdmin)
		//		{
		//			return true;
		//		}

		/*if ($class == 'TaskService')
		{
			switch ($function)
			{
				case 'add':
					$task = $argsArray;
					return UserManager::isUserConnectedToProject($user->userID, $task->projectID);
				case 'addComment':
					$comment = $argsArray;
					return UserManager::isUserConnectedToProject($user->userID, TaskManager::getProjectIDByTaskID($comment->taskID));
				case 'getComments':
					$taskID = $argsArray;
					return UserManager::isUserConnectedToProject($user->userID, TaskManager::getProjectIDByTaskID($taskID));
				case 'set': // Only admins can change tasks
				case 'deleteComment':
				case 'delete':
					return false;
			}
		}
		*/
	}
	public static function hasUserAccessToContent($userID, $contentUID)
	{
		$users = RelationManager::getUsersFromContent($contentUID, true);
		return in_array($userID, $users);
	}
	public static function hasPermission($userID, $permissionName)
	{

		$pdo = BdConn::getDb();
		$stmt = $pdo->prepare("SELECT childID FROM relations WHERE parentTable = 'usersRoles' AND parentID = :userID AND childTable = 'users' AND tag IS NULL");
		$stmt->bindParam(":userID", $userID, PDO::PARAM_INT);
		$stmt->execute();
		$roleIDs = $stmt->fetchAll(PDO::FETCH_COLUMN);

		return "
		SELECT rp.ID
		FROM rolesPermissions AS rp
		JOIN relations AS r ON r.parentTable = 'rolesPermissions' AND r.childTable = 'usersRoles'
			AND r.childID = rp.ID
		WHERE r.parentID IN (" . implode(",", $roleIDs) . ")
		  AND rp.name = :permissionName
	";

		// get the permissions assigned to the roles
		$stmt = $pdo->prepare("
			SELECT rp.ID
			FROM rolesPermissions AS rp
			JOIN relations AS r ON r.parentTable = 'rolesPermissions' AND r.childTable = 'usersRoles'
				AND r.childID = rp.ID
			WHERE r.parentID IN (" . implode(",", $roleIDs) . ")
			  AND rp.name = :permissionName
		");
		$stmt->bindParam(":permissionName", $permissionName, PDO::PARAM_STR);
		$stmt->execute();
		$permissionIDs = $stmt->fetchAll(PDO::FETCH_COLUMN);

		return count($permissionIDs) > 0;
	}
}

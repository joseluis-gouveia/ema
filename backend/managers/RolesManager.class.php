<?php


class RolesManager
{

	/**
	 *
	 * 
	 * @param int (contentUID, roleID)
	 * @return boolean
	 */
	public static function setContentRole($contentUID, $roleID)
	{
		BdConn::runSQL("INSERT INTO contentsRolesPermissions SET usersRolesID = $roleID, contentsUID = $contentUID");
		BdConn::runSQL("UPDATE contents SET locked = 1 WHERE UID = $contentUID");
	}

	/**
	 *
	 * 
	 * @param int (ID)
	 * @return RoleVO
	 */
	public static function getRole($id)
	{
		$sql = ("SELECT ID, role as label FROM usersRoles WHERE ID= $id");
		$query = BdConn::query($sql);
		$row = BdConn::single();
		return new RoleVO($row);
	}
}

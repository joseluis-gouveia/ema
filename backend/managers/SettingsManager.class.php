<?php

class SettingsManager
{


    public static function getKeywords()
    {
        BdConn::query("SELECT settings.keywords FROM settings");
        $row = BdConn::single();

        $keywords = $row['keywords'];
        return $keywords;
    }

    public static function getDescription()
    {
        BdConn::query("SELECT settings.description FROM settings");
        $row = BdConn::single();

        $description = $row['description'];

        return $description;
    }

    public static function getTitle()
    {
        BdConn::query("SELECT settings.title FROM settings");
        $row = BdConn::single();

        $title = $row['title'];

        return htmlspecialchars_decode($title);
    }

    public static function getGateway()
    {
        return GATEWAY;
    }

    public static function getGlobalEmail()
    {
        BdConn::query("SELECT globalEmail FROM settings");
        $row = BdConn::single();

        $result = $row['globalEmail'];

        return $result;
    }

    public static function getAdminName()
    {
        BdConn::query("SELECT adminName FROM settings");
        $row = BdConn::single();

        $result = $row['adminName'];

        return utf8_decode($result);
    }

    public static function getFilesPath()
    {
        return FILESPATH;
    }

    public static function getSiteURL()
    {
        BdConn::query("SELECT siteURL FROM settings");
        $row = BdConn::single();

        $result = $row['siteURL'];

        return $result;
    }

    public static function getAnalytics()
    {
        BdConn::query("SELECT analytics FROM settings");
        $row = BdConn::single();

        $result = $row['analytics'];

        return $result;
    }

    public static function getLinkColor()
    {
        BdConn::query("SELECT linkColor FROM settings");
        $row = BdConn::single();

        $result = $row['linkColor'];

        return $result;
    }

    public static function getOperationalEmail()
    {
        BdConn::query("SELECT operationalEmail FROM settings");
        $row = BdConn::single();

        $result = $row['operationalEmail'];

        return $result;
    }

    public static function getOrdersEmailTemplate()
    {
        BdConn::runSql("SELECT storeOrderEmailTemplate FROM settings");
        $row = BdConn::single();

        $result = $row['storeOrderEmailTemplate'];

        return $result;
    }


    public static function getSettings()
    {
        BdConn::query("SELECT * FROM settings");
        $res = BdConn::single();

        $settings = new SettingsVO($res);

        return $settings;
    }

    public static function getOrdersEmail()
    {
        BdConn::query("SELECT ordersEmail FROM settings");
        $row = BdConn::single();

        $result = $row['ordersEmail'];

        return $result;
    }

    public static function getDatabaseVersion()
    {
        $bdconn = BdConn::connect();
        if (count($bdconn->query("SHOW COLUMNS FROM `settings` LIKE 'databaseVersion'")->fetchAll())) {
            BdConn::query("SELECT databaseVersion FROM settings");
            $row = BdConn::single();

            $result = $row['databaseVersion'];

            return (float) $result;
        }
        else {
            BdConn::runSql('ALTER TABLE settings ADD databaseVersion float(11);');
        }
        
    }
}
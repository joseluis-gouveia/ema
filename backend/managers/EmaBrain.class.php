<?php

/**
 * Singleton class
 *
 */
final class EmaBrain
{

	/**
	 * Public vars
	 *
	 *
	 */
	public static $ALLOW_GET_USERS = 'false';
	public static $ALLOW_EDIT_USERS = 'false';
	public static $ALLOW_ADD_CONTENT_NO_SESSION = 'false';
	public static $ALLOW_ADD_USERS_RELATIONS = 'false';
	public static $SHIPPING_DATA = NULL;
	public static $CURRENTUSER = NULL;

	/**
	 * Call this method to get singleton
	 *
	 * @return EmaBrain
	 */
	public static function Instance()
	{
		static $inst = null;
		if ($inst === null) {
			$inst = new EmaBrain();
		}
		return $inst;
	}



	/**
	 * Private ctor so nobody else can instance it
	 *
	 */
	private function __construct()
	{
	}
}

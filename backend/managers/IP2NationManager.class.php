<?php

class IP2NationManager
{

    /**
     * gets Country by ip
     *
     * @param
     * @return string
     */
    public static function getCountry()
    {
        if ($_SERVER['REMOTE_ADDR'] == "::1" || $_SERVER['REMOTE_ADDR'] == "127.0.0.1") {
            return ["country" => "Portugal"];
        }
        $sql = 'SELECT ip2nationCountries.country, ip2nationCountries.code
    FROM ip2nationCountries INNER JOIN ip2nation ON ip2nationCountries.code = ip2nation.country
    WHERE ip < INET_ATON("' . $_SERVER['REMOTE_ADDR'] . '")
    ORDER BY ip DESC LIMIT 0,1';

        BdConn::query($sql);

        $country = BdConn::single();

        return $country;
    }


    /**
     * gets all Country List
     *
     * @param
     * @return string
     */
    public static function getCountries()
    {
        $sql = 'SELECT ip2nationCountries.country, ip2nationCountries.code, ip2nationCountries.phonecode FROM ip2nationCountries ORDER BY ip2nationCountries.country ASC';
        BdConn::query($sql);
        $list = BdConn::resultset();

        $countries = array();
        foreach ($list as $row) {
            $countries[] = $row;
        }
        return $countries;
    }

    /**
     * gets all Country List
     *
     * @param
     * @return string
     */
    public static function getPhoneCodes()
    {
        $sql = 'SELECT ip2nationCountries.phonecode, ip2nationCountries.country FROM ip2nationCountries ORDER BY ip2nationCountries.phonecode ASC';
        BdConn::query($sql);
        $list = BdConn::resultset();

        $countries = array();
        foreach ($list as $row) {
            $countries[] = $row;
        }
        return $countries;
    }

    /**
     * gets Code by country
     *
     * @param
     * @return string
     */
    public static function getCode($country)
    {

        if (is_array($country)) {
            $country = $country['country'];
        }

        $sql = "SELECT ip2nationCountries.code FROM ip2nationCountries WHERE country = '" . $country . "'";

        BdConn::query($sql);

        $code = BdConn::single();

        return $code;
    }
    /**
     * gets Country by code
     *
     * @param
     * @return string
     */
    public static function getCountryByCode($code)
    {

        $sql = "SELECT ip2nationCountries.country FROM ip2nationCountries WHERE code = '" . $code . "'";

        BdConn::query($sql);

        $code = BdConn::single();

        return $code;
    }
}

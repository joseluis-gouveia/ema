<?php


class BaseSMTP {

	/**
	*
 	* Author: Jose Luis Gouveia
	*/
	const SMTP_SECURE = "tls";
	const HOST = "";
	const USERNAME = "";
	const PASSWORD = "";
	const SMTP_AUTH = false;
	const PORT = "587";
}

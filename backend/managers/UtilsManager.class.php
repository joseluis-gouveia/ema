<?php

class UtilsManager
{



	/**
	 * Get file extension
	 *
	 * @param string
	 * @return string
	 */
	public static function getFileExtension($filename)
	{
		return substr($filename, strrpos($filename, "."));
	}
	/**
	 * Get file name
	 *
	 * @param string
	 * @return string
	 */
	public static function getFileName($filename)
	{
		$names = explode(".", $filename);
		return $names[0];
	}

	/**
	 * Get directory files
	 *
	 * @param string
	 * @return array
	 */
	public static function listDirectoryFiles($dir)
	{
		if (is_dir($dir)) {
			if ($dh = opendir($dir)) {
				$images = array();

				while (($file = readdir($dh)) !== false) {
					if (!is_dir($dir . $file)) {
						$images[] = $file;
					}
				}
				closedir($dh);
				return $images;
			}
		}
		return false;
	}
	/**
	 * remove http
	 *
	 * @param string
	 * @return string
	 */
	public static function remove_http($url = '')
	{
		return (str_replace(array('http://', 'https://'), '', $url));
	}
	/**
	 * adds http
	 *
	 * @param string
	 * @return string
	 */
	public static function addhttp($url)
	{
		if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {
			$url = "http://" . $url;
		}
		return $url;
	}
	/**
	 * remove char from string
	 *
	 *
	 *
	 */
	public static function removeCharFromString($char, $string)
	{
		$newString = str_replace($char, "", $string);
		return $newString;
	}

	/**
	 * Slug Maker
	 *
	 *
	 *
	 */
	public static function makeSlugs($text, $maxlen = 0)
	{
		setlocale(LC_CTYPE, 'pt_PT');
		// replace non letter or digits by -
		$text = preg_replace('~[^\\pL\d]+~u', '-', $text);

		// trim
		$text = trim($text, '-');


		// transliterate
		$text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

		// lowercase
		$text = strtolower($text);

		// remove unwanted characters
		$text = preg_replace('~[^-\w]+~', '', $text);

		if (empty($text)) {
			return 'n-a';
		}
		if ($maxlen > 0) {
			$text = substr($text, 0, $maxlen);
		}

		return $text;
	}
	public static function slugifyFile($filename)
	{
		$extension = self::getFileExtension($filename);
		$nameSlug = self::makeSlugs(substr($filename, 0, strrpos($filename, '.')));
		return $nameSlug . $extension;
	}
	private function removeDuplicates($sSearch, $sReplace, $sSubject)
	{
		$i = 0;
		do {

			$sSubject = str_replace($sSearch, $sReplace, $sSubject);
			$pos = strpos($sSubject, $sSearch);

			$i++;
			if ($i > 100) {
				die('removeDuplicates() loop error');
			}
		} while ($pos !== false);

		return $sSubject;
	}


	/**
	 * zips array of files
	 *
	 *
	 *
	 */

	public static function createZip($files = array(), $destination = CONTENTS, $overwrite = false)
	{
		//if the zip file already exists and overwrite is false, return false

		if (file_exists($destination) && !$overwrite) {
			return true;
		}

		//vars
		$valid_files = array();
		//if files were passed in...

		if (is_array($files)) {
			//cycle through each file
			foreach ($files as $file) {
				//make sure the file exists
				if (file_exists($file)) {
					$valid_files[] = $file;
				}
			}
		}

		//if we have good files...
		if (count($valid_files)) {
			//create the archive
			$zip = new ZipArchive();

			$zip->open($destination, ZipArchive::CREATE);
			// return $zip;
			//add the files
			foreach ($valid_files as $file) {
				$new_filename = substr($file, strrpos($file, '/') + 1);
				$zip->addFile($file, $new_filename);
			}
			//debug
			//return 'The zip archive contains '.$zip->numFiles.' files with a status of '.$zip->status;

			//close the zip -- done!
			$zip->close();

			//check to make sure the file exists
			return file_exists($destination);
		} else {
			return false;
		}
	}


	public static function isStringInString($haystack, $needle)
	{
		$found = false;
		if (strlen(strstr($haystack, $needle)) > 0) {
			$found = true;
		}
		return $found;
	}

	public static function generatePassword($length = 4)
	{

		$chars = "ABCDFGHHJKMMNPPQRSTUVWXYZ9123456789abcdefghijklmnopqrstuvwxyz";
		srand((float)microtime() * 1000000);
		$i = 1;
		$string = '';

		while ($i <= $length) {
			$num = rand() % 51;
			$tmp = substr($chars, $num, 1);
			$string = $string . $tmp;
			$i++;
		}
		return $string;
	}

	public static function getMonth($month, $langID)
	{
		$months = split(";", LabelsManager::getLabelValue("MONTHS", $langID));
		return $months[$month - 1];
	}
	public static function limitString($string, $limit = 100, $continue = "...")
	{
		// Return early if the string is already shorter than the limit
		if (strlen($string) < $limit) {
			return $string;
		}

		$regex = "/(.{1,$limit})\b/";
		preg_match($regex, $string, $matches);
		return $matches[1] . $continue;
	}
	public static function curPageURL()
	{
		$pageURL = 'http';
		if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on") {
			$pageURL .= "s";
		}
		$pageURL .= "://";
		if ($_SERVER["SERVER_PORT"] != "80") {
			$pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"];
		} else {
			$pageURL .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
		}
		return $pageURL;
	}
	public static function scaleDimensions($orig_width, $orig_height, $max_width, $max_height)
	{

		if ($orig_width < $max_width && $orig_height < $max_height) {
			return array($orig_width, $orig_height);
		}

		$ratiow = $max_width / $orig_width;
		$ratioh = $max_height / $orig_height;
		$ratio = min($ratiow, $ratioh);
		$width = intval($ratio * $orig_width);
		$height = intval($ratio * $orig_height);
		return array($width, $height);
	}
	public static function getOnlyDuplicateds($a = array())
	{
		return array_values(array_diff_key($a, array_unique($a)));
	}
	public static function isValidMd5($md5 = '')
	{
		return preg_match('/^[a-f0-9]{32}$/', $md5);
	}
	public static function fixObject(&$object)
	{
		if (!is_object($object) && gettype($object) == 'object')
			return ($object = unserialize(serialize($object)));
		return $object;
	}
	public static function is_serialized($data)
	{
		// if it isn't a string, it isn't serialized
		if (!is_string($data))
			return false;
		$data = trim($data);
		if ('N;' == $data)
			return true;
		if (!preg_match('/^([adObis]):/', $data, $badions))
			return false;
		switch ($badions[1]) {
			case 'a':
			case 'O':
			case 's':
				if (preg_match("/^{$badions[1]}:[0-9]+:.*[;}]\$/s", $data))
					return true;
				break;
			case 'b':
			case 'i':
			case 'd':
				if (preg_match("/^{$badions[1]}:[0-9.E-]+;\$/", $data))
					return true;
				break;
		}
		return false;
	}


	public static function verifyDateFormat($date, $dateFormat, $newDateFormat = NULL)
	{
		//$date="14/04/1982";
		//$dateFormat = "d/m/Y";

		$dt = DateTime::createFromFormat($dateFormat, $date);
		$result = $dt !== false && !array_sum($dt::getLastErrors());
		if (!isset($newDateFormat)) {
			return $result;
		} else {
			$new_date = date($newDateFormat, $dt->getTimestamp());
			return $new_date;
		}
	}

	/**
	 * Get unique List From Array of ContentVO by given field table
	 *
	 * @param array of contentVO
	 * @return array
	 */
	public static function getListFromField($contents, $field)
	{
		$filters = [];
		$field_ = '$field';
		for ($i = 0; $i < count($contents); $i++) {
			$expressions = explode(",", mb_convert_case(trim($contents[$i]->{$field}), MB_CASE_TITLE, "UTF-8"));
			for ($e = 0; $e < count($expressions); $e++) {
				if (!in_array(mb_convert_case(trim($expressions[$e]), MB_CASE_TITLE, "UTF-8"), $filters) && mb_convert_case(trim($expressions[$e]), MB_CASE_TITLE, "UTF-8") != "") {

					$filters[] = mb_convert_case(trim($expressions[$e]), MB_CASE_TITLE, "UTF-8");
				}
			}
		}
		return $filters;
	}

	/**
	 * Format Date
	 *
	 * @param datetime
	 * @return string
	 */
	public static function formatDate($date, $format = 'Y-m-d', $emptyString = "")
	{
		if (isset($_COOKIE['languageID'])) {
			$languageID = $_COOKIE['languageID'];
		} else {
			$languageID = LanguageManager::getDefaultLanguage()->ID;
		}
		$language = LanguageManager::getLanguage($languageID);
		if ($date == "0000-00-00 00:00:00") {
			return $emptyString;
		}

		$dt = new DateTime($date);

		$formatter = new IntlDateFormatter($language->translateCode, IntlDateFormatter::SHORT, IntlDateFormatter::SHORT);
		$formatter->setPattern($format);

		return $formatter->format($dt);
	}

	public static function exportCSV($data, $filename)
	{
		$list = array();

		// Get field names
		$header = array_keys($data[0]);
		array_push($list, $header);

		// Append data to array
		foreach ($data as $row) {
			array_push($list, array_values($row));
		}

		// Output array into CSV file
		$fp = fopen('php://output', 'w');
		header('Content-Type: text/csv');
		header('Content-Disposition: attachment; filename="' . $filename . '"');
		foreach ($list as $ferow) {
			fputcsv($fp, $ferow);
		}
	}

	public static function  mergeArrayData($dataArray)
	{
		$mergedArray = array();
		foreach ($dataArray as $data) {
			$name = $data['name'];
			$value = $data['value'];
			if (array_key_exists($name, $mergedArray)) {
				$mergedArray[$name] .= "," . $value;
			} else {
				$mergedArray[$name] = $value;
			}
		}
		return explode(",", implode(array_values($mergedArray)));
	}

	public static function  mergeArrayChildren($dataArray)
	{
		$mergedArray = array();
		for ($i = 0; $i < count($dataArray); $i++) {
			$mergedArray = array_merge_recursive(explode(",", $dataArray[$i]), $mergedArray);
		}
		return $mergedArray;
	}

	
	public static function convertToCents($amount) {
		// Remove any non-numeric characters except comma and dot
		$amount = preg_replace('/[^0-9.,]/', '', $amount);

		// Convert comma to dot for decimal handling (supports "2,50" as "2.50")
		$amount = str_replace(',', '.', $amount);

		// Convert to float and multiply by 100 to get cents (smallest currency unit)
		$cents = (int) round(floatval($amount) * 100);

		return $cents;
	}



}

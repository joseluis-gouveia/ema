<?php


class LogManager
{

	public static function logAction($userID, $actionID, $websiteID = null, $contentUID = null, $notes = null)
	{
		$url = 0;
		if (isset($_SERVER['HTTP_REFERER']) && $_SERVER['HTTP_REFERER'] != "") {
			$url = $_SERVER['HTTP_REFERER'];
		}
		$sessionID = 0;

		if (isset($_COOKIE['PHPSESSID'])) {
			$sessionID = $_COOKIE['PHPSESSID'];
		}

		date_default_timezone_set("Europe/London");
		$date = date('y-m-d H:i:s');

		if (!isset($websiteID)) {
			$websiteID = 0;
		}
		if (!isset($contentUID)) {
			$contentUID = 0;
		}

		$user_agent = null;
		if (isset($_SERVER['HTTP_USER_AGENT'])) {
			$user_agent = $_SERVER['HTTP_USER_AGENT'];
		}

		$sql = "INSERT INTO log (userID, actionID, date, contentUID, websiteID, IP, OSAndBrowser, notes, sessionID, referer) VALUES (" . $userID . ", " . $actionID . ", '" . $date . "', '" . $contentUID . "', '" . $websiteID . "', '" . $_SERVER['REMOTE_ADDR'] . "', '" . $user_agent . "', '" . $notes . "', '" . $sessionID . "', '" . $url . "')";

		$result = BdConn::runSQL($sql);

		return  $result;
	}
}

<?php

require_once(CLASSESPATH.'/core/ValueObject.class.php');

class ContentTemplateVO extends ValueObject
{
	public $ID;
	public $template;
	public $label;
	
	public $_explicitType= "com.joseluisgouveia.vo.ContentTemplateVO";
	public function __construct($row = null)
	{
		parent::__construct($row);
	}
}

?>
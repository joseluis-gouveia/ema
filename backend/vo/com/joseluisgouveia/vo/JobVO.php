<?php

require_once(CLASSESPATH.'/core/ValueObject.class.php');

class JobVO extends ValueObject
{
	public $ID;
	public $userID;
	public $type;
	public $title;
	public $payment;
	public $reference;
	public $zone;
	public $description;
	public $companyEmail;
	public $companyName;
	public $area;
	public $approved;
	
	public $_explicitType= "com.joseluisgouveia.vo.JobVO";
	public function __construct($row = null)
	{
		parent::__construct($row);
	}
}

?>
<?php

require_once(CLASSESPATH.'/core/ValueObject.class.php');

class FilesVO extends ValueObject
{
	public $default;
	public $imageFiles;
	public $videoFiles;
	public $audioFiles;
	public $otherFiles;
	public $allFiles;
	public $currentFilesExtensions;
	
	public $_explicitType= "com.joseluisgouveia.vo.FilesVO";
	public function __construct($row = null)
	{
		parent::__construct($row);
	}
}

?>
<?php

require_once(CLASSESPATH.'/core/ValueObject.class.php');

class RoleVO extends ValueObject
{
	public $ID;
	public $label;
	
	public $_explicitType= "com.joseluisgouveia.vo.RoleVO";
	public function __construct($row = null)
	{
		parent::__construct($row);
	}
}

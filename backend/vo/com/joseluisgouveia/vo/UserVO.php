<?PHP
require_once(CLASSESPATH.'/core/ValueObject.class.php');

class UserVO extends ValueObject
{
	public $ID;
	public $parentID;
	public $files;
	public $firstName;
	public $lastName;
	public $fullName;
	public $country;
	public $birthday;
	public $email;
	public $job;
	public $address;
	public $zipcode4;
	public $zipcode3;
	public $zipcodeZone;
	public $alternativeZipcode;
	public $shippingContactFirstName;
	public $shippingContactLastName;
	public $shippingAddress;
	public $shippingZipcode4;
	public $shippingZipcode3;
	public $shippingZipcodeZone;
	public $shippingAlternativeZipcode;
	public $shippingCountry;
	public $shippingContactMobile;
	public $invoiceFirstName;
	public $invoiceLastName;
	public $invoiceNif;
	public $invoiceAddress;
	public $invoiceZipcode4;
	public $invoiceZipcode3;
	public $invoiceZipcodeZone;
	public $invoiceAlternativeZipcode;
	public $invoiceCountry;
	public $phone;
	public $mobile;
	public $gender;
	public $optional1;
	public $optional2;
	public $optional3;
	public $optional4;
	public $optional5;
	public $optional6;
	public $optional7;
	public $optional8;
	public $optional9;
	public $optional10;
	public $nif;
	public $newsletter;
	public $partnersPub;
	public $notes;
	public $password;
	public $facebookID;
	public $subscribeDate;
	public $editDate;
	public $expireDate;
	public $activationCode;
	public $active;
	public $approved;
	public $enabled;
	public $roles;



	public $_explicitType= "com.joseluisgouveia.vo.UserVO";

	public function __construct($row = null)
	{
		parent::__construct($row);
	}
}

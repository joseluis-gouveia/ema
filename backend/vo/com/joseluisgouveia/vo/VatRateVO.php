<?php

require_once(CLASSESPATH.'/core/ValueObject.class.php');

class VatRateVO extends ValueObject
{
	public $ID;
	public $langID;
	public $label;
	public $vat;

	public $_explicitType= "com.joseluisgouveia.vo.VatRateVO";
	public function __construct($row = null)
	{
		parent::__construct($row);
	}
}

?>
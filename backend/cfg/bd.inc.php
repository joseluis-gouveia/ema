<?php
		$extension = pathinfo($_SERVER['SERVER_NAME'], PATHINFO_EXTENSION);

		if($extension == "local" || $_SERVER['HTTP_HOST'] == "localhost" || substr($_SERVER['REMOTE_ADDR'],0,8) == "192.168.")
		{
			define("BDSERVER",DEVHOST);
			define("BDUSERNAME",DEVUSERNAME);
			define("BDPASSWORD",DEVPASSWORD);
			define("BDBD",DEVDATABASE);
		}
		else if ($extension == "dev")
		{
			define("BDSERVER",DEVONLINEHOST);
			define("BDUSERNAME",DEVONLINEUSERNAME);
			define("BDPASSWORD",DEVONLINEPASSWORD);
			define("BDBD",DEVONLINEDATABASE);
		}
		else
		{
			define("BDSERVER",CONFIGHOST);
			define("BDUSERNAME",CONFIGUSERNAME);
			define("BDPASSWORD",CONFIGPASSWORD);
			define("BDBD",CONFIGDATABASE);
		}

<?php
function jlg_spl_autoload($class_name) {
	$izi_file=IZICLASSESPATH."/".$class_name.'.class.php';
	$site_file=SITECLASSESPATH."/".$class_name.'.class.php';
	$vo_file=VALUEOBJECTSPATH."/".$class_name.'.php';
	$apis=APIPATH."/".$class_name.'.class.php';
	$plugins = PLUGINSPATH."/".$class_name.'.class.php';
	$plugins_path=PLUGINSPATH;
    $plugin_file = sprintf('%s/%s.php', $plugins_path, str_replace('_', '/', $class_name));
    $phpmailer = PLUGINSPATH.'/PHPMailer/class.phpmailer.php';
	$app_file=APPCLASSPATH."/".$class_name.'.class.php';
	$ics_generator = PLUGINSPATH."/ics-generator/vendor/autoload.php";
	//$payone=APIPATH.'/Payone/Autoload.php';
	
	
	if (file_exists($izi_file))
		require_once $izi_file;
	if (file_exists($apis))
		require_once $apis;
	if (file_exists($site_file))
		require_once $site_file;
	if (file_exists($vo_file))
		require_once $vo_file;
	if (file_exists($plugin_file))
		require_once $plugin_file;
	if (file_exists($plugins))
		require_once $plugins;
	if (file_exists($app_file))
		require_once $app_file;
	if (file_exists($phpmailer))
		require_once $phpmailer;
	if (file_exists($ics_generator))
		require_once $ics_generator; 

	// LEAVE ALWAYS PHPMAILER TO LAST
	
}

spl_autoload_register('jlg_spl_autoload');
<?php

	class Tools {
		
		function devolveDataEmSegundos($data)
		{

			$day=substr($data ,8,2);
		 	$month=substr($data ,5,2);
			$year = substr($data ,0,4);
			
			$res = mktime(0,0,0,$month, $day, $year);
			
			return $res;					 	
		}

		
		function subtraiDatas($data1, $data2)  
		{
			$data1=$this->devolveDataEmSegundos($data1);
			$data2=$this->devolveDataEmSegundos($data2);
			$dias=$data1-$data2;
			$dias/=86400;
			return $dias;
			
		}
		public function birthday ($birthday){
  			list($day,$month,$year) = explode("/",$birthday);
    		$year_diff  = date("Y") - $year;
    		$month_diff = date("m") - $month;
    		$day_diff   = date("d") - $day;
    		if ($day_diff < 0 || $month_diff < 0)
     		 $year_diff--;
    		return $year_diff;
  		}

		function getDataDeHoje() {
			return(date('Y').'-'.date('m').'-'.date('j'));
		}

		/**
		* Valida email;
		*
		* @param string $email
		* @return bool
		*/
		function validaEmail($email) {

			if(eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $email))
				return true;
			else
				return false;
		}
		
		/**
		* Valida Nome (só caracteres e espaços);
		*
		* @param string $nome
		* @return bool
		*/
		function validaNome(&$nome)
    	{
    		trim($nome);

			if(!empty($nome) && eregi("^([a-zA-ZÃÁ?ÀÂÊÈÉÌÌÔÕÒÓÛãáàâêèéìíôõòóûñúùûçÇ]+[[:space:]]*)+$", $nome))
				return true;

			return false;

    	}
    	
    	/**
		* Valida Password (só letras, numeros, _ e - entre x e y caracteres);
		*
		* @param string $pass
		* @return bool
		*/
		function validaPassword(&$pass, $length=8, $limit = 10)
    	{
    		trim($pass);

			if(eregi("^[_a-zA-Z0-9-ÃÁÀÂÊÈÉÌÌÔÕÒÓÛãáàâêèéìíôõòóûñúùûçÇ]{".$length.",".$limit."}$", $pass))
				return true;

			return false;

    	}
    	
    	/**
		* Valida Telefone (só numeros e +)
		*
		* @param string $telefone
		* @return bool
		*/
		function validaTelefone(&$telefone, $length=9, $limit = 9)
    	{
    		trim($telefone);

			if(eregi("^[0-9]{".$length.",".$limit."}$", $telefone))
				return true;

			return false;

    	}
    	
    	/**
     	* Escape a SQL string
     	*/
    	function escape($sql)
    	{
        	$args = func_get_args();
        	foreach($args as $key => $val)
        	{
            	$args[$key] = mysql_real_escape_string($val);
        	}
        
        	$args[0] = $sql;
        	return call_user_func_array('sprintf', $args);
    	} 
    	
    	/**
		* Gera uma password
		*
		* @param int $length Nï¿½ de caracteres de password
		* @return string
		*/
		function generatePassword ($length = 8)
		{
			$password = "";
			$possible = "0123456789bcdfghjkmnpqrstvwxyz";
			$i = 0;
	
			while ($i < $length) {
	
				$char = substr($possible, mt_rand(0, strlen($possible)-1), 1);
	
				if (!strstr($password, $char)) {
				  $password .= $char;
				  $i++;
				}
			}
	
			return $password;
		}
		
		function getFileExtension($file)
		{
			return strtolower(substr($file, strripos($file, '.')));
		}
		
		public function addImage($image, $newPath, $allowedExtensions)
		{

			if ($image==null)
				throw new Exception (ExceptionManager::$PT['IMAGE_PATH_NOT_FOUND']);
			
			$extension=Tools::getFileExtension($image);
						
			$valid_extension=false;
			for ($i=0; $i<count($allowedExtensions); ++$i)
			{
				if (strtolower($extension)==$allowedExtensions[$i])
				{
					$valid_extension=true;
					break;
				}
				
			}
			if (!$valid_extension)
				throw new Exception (ExceptionManager::$PT['INVALID_EXTENSION']);
			
			if (!file_exists($newPath))
				mkdir($newPath); 
			
			$newname=(time() . rand(999, 999999));

			$newFile=$newPath. '/'. $newname . $extension;

			if (!copy($image, $newFile))				
				throw new Exception (ExceptionManager::$PT['FILE_COPY_FAILED']);
		
			unlink($image);
			
			return $newname. $extension;
		}
		
		public function explodeFont($mensagem)
		{ 
			$msg = explode('FACE="Verdana"',$mensagem);
			
			for($x=0; $x<count($msg); $x++){ 
				$msgFinal .= $msg[$x];
			} 
			return $msgFinal;
		}
		
		function db_escape($values) {
			
			if (is_object($values)) {
		        foreach ($values as $key => $value) {
		            $values->$key = Tools::db_escape($value);
		        }
		    }
		    else if (is_array($values)) {
		        foreach ($values as $key => $value) {
		            $values[$key] = Tools::db_escape($value);
		        }
		    }
		    else if ($values === null) {
		        $values = 'NULL';
		    }
		    else if (is_bool($values)) {
		        $values = $values ? 1 : 0;
		    }
		    else if (!is_numeric($values)) {
		       // $values = mysql_real_escape_string($values);
		    }
		    return $values;
		}
		
		function byteToString($bytes)
	    {
	        $s = array('B', 'Kb', 'MB', 'GB', 'TB', 'PB');
	        $e = floor(log($bytes)/log(1024));
	       
	        return sprintf('%.2f '.$s[$e], ($bytes/pow(1024, floor($e))));
	    }
		
		var $imageExtensions=array(0 => '.jpg', 1 => '.png',2 => '.gif');
	

	}
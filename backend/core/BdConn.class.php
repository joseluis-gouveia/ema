<?php

/**
 * Classe responsável por simplificar todas as tarefas de acesso e manipulação da base de dados; Esta classe pode ser instanciada ou utilizada como um solito
 *
 * @package IZI
 * @author webfuel
 * @version 0.1
 * @todo Completar com os métodos restantes
 */


/**
 * Classe de conexão ao banco de dados usando PDO no padrão Singleton.
 * Modo de Usar:
 * require_once '/bd.inc.php';
 * $db = BdConn::connect();
 * E agora use as funções do PDO (prepare, query, exec) em cima da variável $db.
 */
require_once(CFGPATH . "/bd.inc.php");

class BdConn
{
    # Variável que guarda a conexão PDO.
    protected static $db;
    protected static $stmt;
    protected static $BDBD = BDBD;
    protected static $BDPASSWORD = BDPASSWORD;
    protected static $BDUSERNAME = BDUSERNAME;
    protected static $BDSERVER = BDSERVER;

    # Private construct - garante que a classe só possa ser instanciada internamente.
    private function __construct()
    {
        # Informações sobre a base de dados:
        $dbname = self::$BDBD;
        $password = self::$BDPASSWORD;
        $username = self::$BDUSERNAME;
        $host = self::$BDSERVER;

        # tipo de ligação
        $db_type = "mysql";

        # Informações sobre o sistema:
        $admin_name = "EMA";
        $admin_email = "log@joseluisgouveia.com";

        try {
            # Atribui o objeto PDO à variável $db.
            self::$db = new PDO("$db_type:host=$host; dbname=$dbname; charset=utf8;", $username, $password);

            # Garante que o PDO lance exceções durante erros.
            self::$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            # Garante que os dados sejam armazenados com codificação UFT-8.
            self::$db->exec('SET NAMES utf8');
        } catch (PDOException $e) {
            # Envia um e-mail para o e-mail oficial do sistema, em caso de erro de conexão.
            mail($admin_email, "PDOException em $admin_name", $e->getMessage());

            # Então não carrega nada mais da página.
            die("Connection Error: " . $e->getMessage());
        }
    }

    # Método estático - acessível sem instanciação.
    public static function connect()
    {
        # Garante uma única instância. Se não existe uma conexão, criamos uma nova.
        if (!self::$db) {
            new BdConn();
        }

        # Retorna a conexão.
        return self::$db;
    }

    public static function query($sqlstatement)
    {
        # faz a ligação
        $bdconn = BdConn::connect();

        # prepara o SQL
        self::$stmt = $bdconn->prepare($sqlstatement);

        # Executa o SQL
        self::$stmt->execute();

        # Foi processado
        return true;
    }

    public static function single($obj = false)
    {
        if (self::$db && self::$stmt) {
            if ($obj) {
                # Devolve a resposta em Object
                return self::$stmt->fetch(PDO::FETCH_CLASS | PDO::FETCH_CLASSTYPE);
            } else {
                # Devolve a resposta em Array
                return self::$stmt->fetch(PDO::FETCH_ASSOC);
            }
        } else {
            return false;
        }
    }

    public static function resultset($obj = false)
    {
        if (self::$db && self::$stmt) {
            if ($obj) {
                # Devolve a resposta em Object
                return self::$stmt->fetchAll(PDO::FETCH_CLASS | PDO::FETCH_CLASSTYPE);
            } else {
                # Devolve a resposta em Array
                return self::$stmt->fetchAll(PDO::FETCH_ASSOC);
            }
        } else {
            return false;
        }
    }

    public static function getLastInsertId()
    {
        if (self::$db) {
            return self::$db->lastInsertId();
        } else {
            return false;
        }
    }

    public static function runSQL($sqlstatement, $obj = false)
    {
        # faz a ligação
        $bdconn = BdConn::connect();

        # prepara o SQL
        $stmt = $bdconn->prepare($sqlstatement);

        # Executa o SQL
        $exec = $stmt->execute();

        # Primeiro Char do SQL para identificar o tipo execução
        $type = substr(trim($sqlstatement), 0, 1);

        switch ($type) {
            case "S":
                if ($obj) {
                    # Devolve a resposta em Object
                    $response = $stmt->fetchAll(PDO::FETCH_CLASS | PDO::FETCH_CLASSTYPE);
                } else {
                    # Devolve a resposta em Array
                    $response = $stmt->fetchAll(PDO::FETCH_ASSOC);
                }

                return $response;
            default:
                return $exec;
        }
    }
    public static function tableExists($table)
    {

        // Try a select statement against the table
        // Run it in try/catch in case PDO is in ERRMODE_EXCEPTION.
        $pdo = BdConn::connect();
        try {
            $result = $pdo->query("SELECT 1 FROM $table LIMIT 1");
        } catch (Exception $e) {
            // We got an exception == table not found
            return FALSE;
        }

        // Result is either boolean FALSE (no table found) or PDOStatement Object (table found)
        return $result !== FALSE;
    }

    /**
     * Devolve a row seguinte de um resource
     *
     * @param resource $resultset
     * @return array
     */
    static function next($resultset)
    {
        return $resultset;
    }


    /**
     * Devolve uma array com a estrutura duma tabela MySQL.
     *
     * @param string $table
     * @return array   [nome][length]
     */
    static function getStruct($table, $id_field_name = 'id')
    {

        $res = BdConn::runSQL("SELECT * FROM $table WHERE $id_field_name<0");
        $num_fields = count($res);
        $pos = 0;
        $row = '';
        while ($pos != $num_fields) {

            $row[mysql_field_name($res, $pos)]['length'] = mysql_field_len($res, $pos);
            ++$pos;
        }

        return $row;
    }

    /**
     * Descarrega uma array indexada para uma tabela (gerando dinamicamente um comando SQL), se os nomes da array passada coincidirem. Devolve o valor do ID
     *
     * @param array $row
     * @param string $tabela
     * @param string $id_field_name = 'id' Nome da chave primaria da tabela (se tiver auto_increment)
     * @return int
     */
    static function add($row, $table, $id_field_name = 'ID')
    {

        if (!is_array($row))
            throw new Exception('Devera ser recebida uma row para descarregar na BD');
        else {

            reset($row);

            $column_names = $values = ' ';
            $pos = 0;

            $struct = BdConn::getStruct($table, $id_field_name);

            while (list($key, $val) = each($row)) {

                if (array_key_exists($key, $struct)) {
                    if ($key != $id_field_name) {

                        ++$pos;
                        if ($pos != 1) {
                            $column_names = $column_names . ', ';
                            $values = $values . ', ';
                        }

                        $column_names = $column_names . $key;

                        if (!is_int($val) and $val == "\null")
                            $values = $values . "NULL";
                        else {
                            if (is_int($val))
                                $values = $values . $val;
                            else
                                $values = $values . "'" . $val . "'";
                        }
                    }
                }
            }

            $sqlstatement = "INSERT INTO $table ($column_names) VALUES ($values)";

            BdConn::runSQL($sqlstatement, false);

            $row = BdConn::runSQL('SELECT last_insert_id() AS ID');

            return $row[$id_field_name];
        }
    }

    /**
     * Altera os valores de uma tabela MySQL de acordo com os recebidos numa row indexada. Para for�ar um campo a NULL, usar a string \null
     *
     * @param array $row
     * @param string $tabela
     * @param string $id_field_name = 'id' Nome da chave primaria da tabela (se tiver auto_increment)
     * @return bool
     */
    static function set($row, $table, $id_field_name = 'ID')
    {

        if (!is_array($row))
            throw new Exception('Deveré ser recebida uma row para descarregar na BD');
        if ($row[$id_field_name] == null)
            throw new Exception('A row recebida deve ter uma coluna com os valores das chaves primárias das linhas a alterar');

        if (is_array($row) and $row[$id_field_name] != null) {

            reset($row);

            $clause = ' ';
            $pos = 0;
            $struct = BdConn::getStruct($table, $id_field_name);

            while (list($key, $val) = each($row)) {

                ++$pos;

                if (array_key_exists($key, $struct)) {
                    if ($key != $id_field_name) {

                        $clause = $clause . "$key=";

                        if (!is_int($val) and $val == "\null") {
                            //                          echo $val;
                            $clause = $clause . "NULL";
                        } else
                            if (is_int($val))
                            $clause = $clause . $val;
                        else
                            $clause = $clause . "'" . $val . "'";

                        $clause = $clause . ', ';
                    }
                }
            }

            $clause = substr($clause, 0, strlen($clause) - 2);

            $sqlstatement = "UPDATE $table SET $clause WHERE ID=" . $row[$id_field_name];
            BdConn::runSQL($sqlstatement, false);
            return true;
        }
        return false;
    }

    /**
     * Apaga uma linha da tabela, onde o ID=$id
     *
     * @param int $id
     * @param string $tabela
     */
    static function delPhysical($id, $tabela, $id_field_name = 'id')
    {

        BdConn::runSQL("DELETE FROM $tabela WHERE $id_field_name=$id", false);
    }

    /**
     * Altera a 'visibilidade' de um registo MySQL
     *
     * @param int $id
     * @param bool $visibilidade
     * @param string $tabela
     * @param string $id_field_name = 'id'
     */
    static function setVisible($id, $visibilidade, $tabela, $id_field_name = 'ID')
    {

        BdConn::runSQL("UPDATE $tabela SET enabled=$visibilidade WHERE $id_field_name=$id", false);

        if ($visibilidade)
            return true;
        else
            return false;
    }

    /**
     * Devolve uma array correspondente a linha a consultar na tabela para determinado id
     *
     * @param int $int
     * @param string $tabela
     * @param string $id_field_name
     * @return array
     */
    static function get($id, $tabela, $id_field_name = 'id')
    {
        $res = BdConn::runSQL("SELECT * FROM $tabela WHERE $id_field_name=" . $id);
        return $res;
    }

    /**
     * Devolve uma lista (resultset) com todas as linhas de uma determinada tabela que repeitem a condi�ao $whereClause, onde visible seja $visible, ordenados por $orderby
     *
     * @param string $tabela
     * @param string $whereClause
     * @param string $orderby
     * @return resource
     */
    static function getList($tabela, $whereClause = '', $orderby = '')
    {

        if ($whereClause != "")
            $whereClause = " WHERE " . $whereClause;

        if ($orderby != "")
            $orderby = " ORDER BY " . $orderby;


        $sqlstatement = "SELECT * FROM  $tabela $whereClause $orderby";
        return BdConn::runSQL($sqlstatement);
    }

    /**
     * Devolve a quantidade de elementos na tabela $table, cujo $field � $value. Tem ainda o $where para se for preciso completar o comando Se $key=false, entao conta todos os elementos da tabela
     *
     * @param string $table
     * @param string $field = ''
     * @param string $value = ''
     * @param string $where = '' - Serve para fazer consultas mais personalizadas. N�o necessita do primeiro and
     * @return int
     */
    static function getCount($table, $field = '', $value = '', $where = '')
    {
        if ($where != '')
            $where = ' and ' . $where;

        if ($field == '')
            $row = BdConn::runSQL("SELECT COUNT(*) AS quantidade FROM $table");
        else
            $row = BdConn::runSQL("SELECT COUNT(*) AS quantidade FROM $table WHERE $field='$value' $where");

        return $row['quantidade'];
    }

    /**
     * Devolve o valor do campo $field, para o id $id, na tabela $table
     *
     * @param string $table
     * @param string $field
     * @param int $id
     * @param string $where = '' - Serve para fazer consultas mais personalizadas. N�o necessita do primeiro AND
     * @param string $id_field_name = 'id' - Nome da chave primaria da tabela
     * @return mixed
     */
    static function getFieldValue($table, $field, $id = '', $where = '', $field_name = 'id')
    {
        if ($id != '')
            $row = BdConn::runSQL("SELECT $field FROM $table WHERE $field_name=$id");
        else
            $row = BdConn::runSQL("SELECT $field FROM $table WHERE $where");

        return $row[$field];
    }

    public static function getDb()
    {
        return self::$db;
    }
}

<?php

	class AuthTools 
	{
			
		public function hasAccess()
		{
			$loginVO=new UserVO();
			$loginVO->username=$_SESSION["username"];
			$loginVO->password=$_SESSION["password"];
			$loginVO->type=$_SESSION["type"];
			$loginVO->isPress=$_SESSION["isPRess"];

			$ut=new UserManager();
			
			if (!$ut->doLogin($loginVO))
			{
				return false;
			}
			else 
			{
				return true;
			}
			
		}
		public function hasPressAccess()
		{
			$loginVO=new UserVO();
			$loginVO->username=$_SESSION["username"];
			$loginVO->password=$_SESSION["password"];
			$loginVO->type=$_SESSION["type"];
			$loginVO->isPress=$_SESSION["isPRess"];

			$ut=new UserManager();
			
			if (!$ut->doPressLogin($loginVO))
			{
				return false;
			}
			else 
			{
				return true;
			}
			
		}


	}